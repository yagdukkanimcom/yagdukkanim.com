
from django import forms
from .models import Category, SubCategory, SubCategory2

TOP_CHOICES = [
    ('Normal', 'Normal'),
    ('Top', 'Top'),
]


class AddCategoryForm(forms.ModelForm):
    category_name = forms.CharField(max_length=80, widget=forms.TextInput(attrs={'rows':5,'class': 'form-control', 'placeholder': 'Kategori Adı'}))
    top_category = forms.ChoiceField(
        widget=forms.RadioSelect,
        choices = TOP_CHOICES,
        required=False,
    )

    class Meta:
        model = Category
        fields = ['category_name','top_category']

class AddSubCategoryForm(forms.ModelForm):
    sub_category_name = forms.CharField(max_length=80, widget=forms.TextInput(attrs={'rows':5,'class': 'form-control', 'placeholder': 'Alt Kategori Adı'}))
    top_category = forms.ChoiceField(
        widget=forms.RadioSelect,
        choices = TOP_CHOICES,
        required=False,
    )

    class Meta:
        model = SubCategory
        fields = ['sub_category_name','top_category']

class AddSubCategory2Form(forms.ModelForm):
    sub_category2_name = forms.CharField(max_length=80, widget=forms.TextInput(attrs={'rows':5,'class': 'form-control', 'placeholder': '2. Alt Kategori'}))
    top_category = forms.ChoiceField(
        widget=forms.RadioSelect,
        choices = TOP_CHOICES,
        required=False,
    )

    class Meta:
        model = SubCategory2
        fields = ['sub_category2_name','top_category']

