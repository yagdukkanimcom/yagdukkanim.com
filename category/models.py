from django.db import models
from django.template.defaultfilters import slugify
from django.shortcuts import reverse
from unidecode import unidecode

class Category(models.Model):
    category_name = models.CharField(max_length=50)
    slug = models.SlugField(null=True, unique=True, editable=False)
    top_category = models.CharField(max_length=70, null=True, blank=True)

    def __str__(self):
        return self.category_name

    class Meta:
        verbose_name_plural = "Kategori"

    def get_sub_category(self):
        return self.sub_category.all().order_by('sub_category_name')
    
    def get_unique_slug(self):
        sayi=0
        slug = slugify(unidecode(self.category_name))
        new_slug=slug
        while Category.objects.filter(slug=new_slug).exists():
            sayi+=1
            new_slug="%s-%s"%(slug,sayi)
        slug = new_slug
        return slug

    def save(self, *args, **kwargs):
        if self.id is None:
            category_name = self.get_unique_slug()
            self.slug = slugify(unidecode(category_name))
        else:
            category=Category.objects.get(slug=self.slug)
            if category.category_name != self.category_name:
                self.slug=self.get_unique_slug()
        super(Category,self).save()


class SubCategory(models.Model):
    sub_category_name = models.CharField(max_length=50)
    category = models.ForeignKey('Category', related_name='sub_category' ,on_delete=models.CASCADE, blank=True, null=True)
    slug = models.SlugField(null=True, unique=True, editable=False)
    top_category = models.CharField(max_length=70, null=True, blank=True)

    def __str__(self):
        return self.sub_category_name
    class Meta:
        verbose_name_plural = "Alt Kategori"

    def get_sub_category2(self):
        return self.sub_category2.all().order_by('sub_category2_name')

    def get_unique_slug(self):
        sayi=0
        slug = slugify(unidecode(self.sub_category_name))
        new_slug=slug
        while SubCategory.objects.filter(slug=new_slug).exists():
            sayi+=1
            new_slug="%s-%s"%(slug,sayi)
        slug = new_slug
        return slug

    def save(self, *args, **kwargs):
        if self.id is None:
            sub_category_name = self.get_unique_slug()
            self.slug = slugify(unidecode(sub_category_name))
        else:
            sub_category=SubCategory.objects.get(slug=self.slug)
            if sub_category.sub_category_name != self.sub_category_name:
                self.slug=self.get_unique_slug()
        super(SubCategory,self).save()


class SubCategory2(models.Model):
    sub_category2_name = models.CharField(max_length=50)
    sub_category = models.ForeignKey('SubCategory', related_name='sub_category2' ,on_delete=models.CASCADE, blank=True, null=True)
    slug = models.SlugField(null=True, unique=True, editable=False)
    top_category = models.CharField(max_length=70, null=True, blank=True)

    def __str__(self):
        return self.sub_category2_name
    class Meta:
        verbose_name_plural = "Altının Altı Kategori"

    def get_unique_slug(self):
        sayi=0
        slug = slugify(unidecode(self.sub_category2_name))
        new_slug=slug
        while SubCategory2.objects.filter(slug=new_slug).exists():
            sayi+=1
            new_slug="%s-%s"%(slug,sayi)
        slug = new_slug
        return slug

    def save(self, *args, **kwargs):
        if self.id is None:
            sub_category2_name = self.get_unique_slug()
            self.slug = slugify(unidecode(sub_category2_name))
        else:
            sub_category2=SubCategory2.objects.get(slug=self.slug)
            if sub_category2.sub_category2_name != self.sub_category2_name:
                self.slug=self.get_unique_slug()
        super(SubCategory2,self).save()



class Size(models.Model):
    size_name = models.CharField(max_length=50)
    slug = models.SlugField(null=True, unique=True, editable=False)

    def __str__(self):
        return self.size_name
    
    class Meta:
        verbose_name_plural = "Size"

    def get_unique_slug(self):
        sayi=0
        slug = slugify(unidecode(self.size_name))
        new_slug=slug
        while Size.objects.filter(slug=new_slug).exists():
            sayi+=1
            new_slug="%s-%s"%(slug,sayi)
        slug = new_slug
        return slug

    def save(self, *args, **kwargs):
        if self.id is None:
            size_name = self.get_unique_slug()
            self.slug = slugify(unidecode(size_name))
        else:
            size=Size.objects.get(slug=self.slug)
            if size.size_name != self.size_name:
                self.slug=self.get_unique_slug()
        super(Size,self).save()


class Color(models.Model):
    color_name = models.CharField(max_length=50)
    slug = models.SlugField(null=True, unique=True, editable=False)

    def __str__(self):
        return self.color_name
    

    def get_unique_slug(self):
        sayi=0
        slug = slugify(unidecode(self.color_name))
        new_slug=slug
        while Color.objects.filter(slug=new_slug).exists():
            sayi+=1
            new_slug="%s-%s"%(slug,sayi)
        slug = new_slug
        return slug

    def save(self, *args, **kwargs):
        if self.id is None:
            color_name = self.get_unique_slug()
            self.slug = slugify(unidecode(color_name))
        else:
            size=Color.objects.get(slug=self.slug)
            if size.color_name != self.color_name:
                self.slug=self.get_unique_slug()
        super(Color,self).save()

    class Meta:
        verbose_name_plural = "Color"
