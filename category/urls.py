from django.urls import path
from . import views
from django.conf.urls import url

app_name = 'category'

urlpatterns = [

    path('add-list-category/', views.add_list_category, name="add-list-category"),
    path('delete-category/<int:pk>/', views.delete_category, name="delete-category"),
    path('update-category/<int:pk>/', views.update_category, name="update-category"),

    path('add-list-subcategory/', views.add_list_subcategory, name="add-list-subcategory"),
    path('delete-subcategory/<int:pk>/', views.delete_sub_category, name="delete-subcategory"),
    path('update-subcategory/<int:pk>/', views.update_sub_category, name="update-subcategory"),

    path('add-list-subcategory2/', views.add_list_subcategory2, name="add-list-subcategory2"),
    path('delete-subcategory2/<int:pk>/', views.delete_sub_category2, name="delete-subcategory2"),
    path('update-subcategory2/<int:pk>/', views.update_sub_category2, name="update-subcategory2"),


    
]
