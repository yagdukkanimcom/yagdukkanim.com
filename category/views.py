from django.shortcuts import render, redirect, get_object_or_404
from django.contrib import messages
from accounts.views import get_auth
from .models import Category, SubCategory, SubCategory2
from .forms import AddCategoryForm, AddSubCategoryForm, AddSubCategory2Form

def add_list_category(request):
    flag, gidilecek_sayfa = get_auth(request)
    if flag:
        return redirect(gidilecek_sayfa)

    categories = Category.objects.all()
    if request.POST:
        form = AddCategoryForm(request.POST)
        if form.is_valid():
            form.save()
            messages.success(request, 'Kategori Ekleme İşlemi Başarılı')
            return redirect('category:add-list-category')
        else:
            messages.error(request, 'Kategori Ekleme İşlemi Başarısız')
    else:
        form = AddCategoryForm()

    context = {'form': form, 'categories':categories ,'pageheader': 'Kategori Ekle'}
    return render(request, 'adminpanel/category/add_list_category.html', context)

def add_list_subcategory(request):
    flag, gidilecek_sayfa = get_auth(request)
    if flag:
        return redirect(gidilecek_sayfa)

    categories = Category.objects.all()
    sub_categories = SubCategory.objects.all()

    if request.POST:
        form = AddSubCategoryForm(request.POST)
        category_pk = request.POST.get('category_pk')
        category = get_object_or_404(Category, pk = category_pk)
        if form.is_valid():
            a = form.save(commit=False)
            a.category = category
            a.save()

            messages.success(request, 'Kategori Ekleme İşlemi Başarılı')
            return redirect('category:add-list-subcategory')
        else:
            messages.error(request, 'Kategori Ekleme İşlemi Başarısız')
    else:
        form = AddSubCategoryForm()

    context = {'form': form, 'categories':categories, 'sub_categories':sub_categories ,'pageheader': 'Alt Kategori Ekle'}
    return render(request, 'adminpanel/category/add_list_sub_category.html', context)

def add_list_subcategory2(request):
    flag, gidilecek_sayfa = get_auth(request)
    if flag:
        return redirect(gidilecek_sayfa)

    sub_categories = SubCategory.objects.all()
    sub_categories_2 = SubCategory2.objects.all()
    if request.POST:
        form = AddSubCategory2Form(request.POST)
        sub_category_pk = request.POST.get('sub_category_pk')
        sub_category = get_object_or_404(SubCategory, pk = sub_category_pk)
        if form.is_valid():
            a=form.save(commit=False)
            a.sub_category = sub_category
            a.save()
            messages.success(request, 'Kategori Ekleme İşlemi Başarılı')
            return redirect('category:add-list-subcategory2')
        else:
            messages.error(request, 'Kategori Ekleme İşlemi Başarısız')
    else:
        form = AddSubCategory2Form()
    

    context = {'form': form, 'sub_categories':sub_categories, 'sub_categories_2':sub_categories_2 ,'pageheader': 'Alt Kategori 2 Ekle'}
    return render(request, 'adminpanel/category/add_list_sub_category_2.html', context)



def update_category(request, pk):
    flag, gidilecek_sayfa = get_auth(request)
    if flag:
        return redirect(gidilecek_sayfa)
    
    category = get_object_or_404(Category, pk=pk)
    form = AddCategoryForm(instance=category, data=request.POST or None)
    if request.POST:
        if form.is_valid():
            form.save()
            messages.success(request, 'Güncelleme İşlemi Başarılı')
            return redirect('category:add-list-category')
        else:
            messages.error(request, 'Güncelleme İşlemi Başarısız')


    context = {'form': form, 'pageheader': 'Kategori Güncelle'}
    return render(request, 'adminpanel/category/update_category.html', context)

def update_sub_category(request, pk):
    flag, gidilecek_sayfa = get_auth(request)
    if flag:
        return redirect(gidilecek_sayfa)
    
    sub_category = get_object_or_404(SubCategory, pk=pk)
    form = AddSubCategoryForm(instance=sub_category, data=request.POST or None)
    if request.POST:
        if form.is_valid():
            form.save()
            messages.success(request, 'Güncelleme İşlemi Başarılı')
            return redirect('category:add-list-subcategory')
        else:
            messages.error(request, 'Güncelleme İşlemi Başarısız')


    context = {'form': form, 'pageheader': 'Alt Kategori Güncelle'}
    return render(request, 'adminpanel/category/update_subcategory.html', context)


def update_sub_category2(request, pk):
    flag, gidilecek_sayfa = get_auth(request)
    if flag:
        return redirect(gidilecek_sayfa)
    
    sub_category2 = get_object_or_404(SubCategory2, pk=pk)
    form = AddSubCategory2Form(instance=sub_category2, data=request.POST or None)
    if request.POST:
        if form.is_valid():
            form.save()
            messages.success(request, 'Güncelleme İşlemi Başarılı')
            return redirect('category:add-list-subcategory2')
        else:
            messages.error(request, 'Güncelleme İşlemi Başarısız')


    context = {'form': form, 'pageheader': 'Alt Kategori 2 Güncelle'}
    return render(request, 'adminpanel/category/update_sub_category2.html', context)


def delete_sub_category2(request, pk):
    flag, gidilecek_sayfa = get_auth(request)
    if flag:
        return redirect(gidilecek_sayfa)

    sub_category2 = get_object_or_404(SubCategory2, pk=pk)
    sub_category2.delete()
    messages.success(request, 'Silme İşlemi Başarılı')
    return redirect('category:add-list-subcategory2')

def delete_sub_category(request, pk):
    flag, gidilecek_sayfa = get_auth(request)
    if flag:
        return redirect(gidilecek_sayfa)

    sub_category = get_object_or_404(SubCategory, pk=pk)
    sub_category.delete()
    messages.success(request, 'Silme İşlemi Başarılı')
    return redirect('category:add-list-subcategory')

def delete_category(request, pk):
    flag, gidilecek_sayfa = get_auth(request)
    if flag:
        return redirect(gidilecek_sayfa)

    category = get_object_or_404(Category, pk=pk)
    category.delete()
    messages.success(request, 'Silme İşlemi Başarılı')
    return redirect('category:add-list-category')