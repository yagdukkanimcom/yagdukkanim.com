from django.contrib import admin
from .models import Category, SubCategory, SubCategory2, Color, Size

admin.site.register(Category)
admin.site.register(SubCategory)
admin.site.register(SubCategory2)

admin.site.register(Size)
admin.site.register(Color)
