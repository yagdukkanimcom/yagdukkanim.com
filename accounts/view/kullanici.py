from django.shortcuts import render, redirect, get_object_or_404, reverse
from django.contrib.auth import authenticate, login, logout
from django.core.exceptions import ObjectDoesNotExist
from django.contrib.auth import update_session_auth_hash
from django.contrib import messages

from ..forms import UserRegisterForm, UserUpdateForm, UserPasswordChangeForm, KullaniciRegisterForm
from ..models import Kullanici
from ..views import get_auth


def kullanici_password_change(request):
    flag, gidilecek_sayfa = get_auth(request)
    if flag:
        return redirect(gidilecek_sayfa)

    form = UserPasswordChangeForm(user=request.user, data=request.POST or None)
    if request.POST:
        if form.is_valid():
            new_password = form.cleaned_data.get('new_password')
            request.user.set_password(new_password)
            request.user.save()
            update_session_auth_hash(request, request.user)
            messages.success(request, 'Şifre Güncelleme İşlemi Başarılı')
            return redirect('accounts:kullanici-password-change')
        else:
            messages.error(request, 'Şifre Güncelleme İşlemi Başarısız')
    context = {'form':form, 'pageheader': 'Kullanıcı Parola Değiştirme'}
    return render(request, 'accounts/kullanici/kullanici_password_change.html', context)


def kullanici_register(request):
    flag, gidilecek_sayfa = get_auth(request)
    if flag:
        return redirect(gidilecek_sayfa)

    if request.POST:
        form = UserRegisterForm(request.POST)
        kullanici_form = KullaniciRegisterForm(request.POST)
        if form.is_valid() and kullanici_form.is_valid():
            user = form.save()
            kullanici = kullanici_form.save(commit=False)
            kullanici.user = user
            kullanici.save()
            username = form.cleaned_data.get('username')
            password = form.cleaned_data.get('password1')
            user = authenticate(username=username, password=password)
            login(request, user)
            messages.success(request, 'Kullanıcı Ekleme İşlemi Başarılı')
            return redirect('accounts:kullanici-list')
        else:
            messages.error(request, 'Kullanıcı Ekleme İşlemi Başarısız')
    else:
        form = UserRegisterForm()
        kullanici_form = KullaniciRegisterForm()
    context = {'form': form, 'kullanici_form': kullanici_form, 'pageheader': 'Kullanıcı Kayıt İşlemi'}
    return render(request, 'accounts/kullanici/register/register.html', context)


def kullanici_update(request):
    flag, gidilecek_sayfa = get_auth(request)
    if flag:
        return redirect(gidilecek_sayfa)

    kullanici = get_object_or_404(Kullanici, user=request.user)
    form = UserUpdateForm(instance=kullanici.user, data=request.POST or None)

    if request.POST:
        if form.is_valid():
            form.save()
            messages.success(request, 'Güncelleme İşlemi Başarılı')
            return redirect('adminpanel:index')
        else:
            messages.error(request, 'Güncelleme İşlemi Başarısız')

    context = {'form': form, 'pageheader': 'Kullanıcı Bilgilerini Güncelle'}
    return render(request, 'accounts/kullanici/kullanici_update.html', context)