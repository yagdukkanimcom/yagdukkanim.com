from django import forms
from django.contrib.auth.models import User
from django.contrib.auth.forms import UserCreationForm, UserChangeForm
from django.contrib.auth import authenticate
from .models import Superadmin, Kullanici, Duty, CompanyImage, CompanyInfo
from client.models import *
import re

class UserRegisterForm(UserCreationForm):

    email = forms.EmailField(max_length=60, widget=forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'Email', 'type':'email'}))
    username = forms.CharField(max_length=100, widget=forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'Kullanıcı Adı'}))
    first_name = forms.CharField(max_length=100, widget=forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'Adı'}))
    last_name = forms.CharField(max_length=100, widget=forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'Soyadı'}))
    password1 = forms.CharField(widget=forms.PasswordInput(attrs={'class': 'form-control', 'placeholder': 'Parola'}))
    password2 = forms.CharField(widget=forms.PasswordInput(attrs={'class': 'form-control', 'placeholder': 'Parola Tekrar'}))

    class Meta:
        model = User
        fields=('first_name', 'last_name', 'username', 'email', 'password1', 'password2')

    def save(self, commit=True):
        user = super().save(commit=False)

        user.email = self.cleaned_data['email']
        user.first_name = self.cleaned_data['first_name']
        user.last_name = self.cleaned_data['last_name']

        if commit:
            user.save()
        return user

class UserUpdateForm(UserChangeForm):
    first_name = forms.CharField(max_length=60, widget=forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'Vorname'}))
    last_name = forms.CharField(max_length=60, widget=forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'Nachname'}))
    email = forms.EmailField(max_length=60, widget=forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'Email', 'type':'email'}))
    username = forms.CharField(max_length=100, widget=forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'Nutzername'}))

    class Meta:
        model = User
        fields=('first_name','last_name','email', 'username', 'password')


class LoginForm(forms.ModelForm):
    username = forms.CharField(max_length=60, widget=forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'Nutzername'}))
    password = forms.CharField(widget=forms.PasswordInput(attrs={'class': 'form-control', 'placeholder': 'Passwort'}))
    #captcha = ReCaptchaField()

    class Meta:
        model = User
        fields = ('username','password')

    def clean(self):
        if self.is_valid():
            username = self.cleaned_data['username']
            password = self.cleaned_data['password']
            if not authenticate(username=username, password=password):
                raise forms.ValidationError('Hatalı Giriş')

class UserPasswordChangeForm(forms.Form):
    user = None
    old_password = forms.CharField(widget=forms.PasswordInput(attrs={'class': 'form-control', 'placeholder': 'Altes Passwort'}))
    new_password = forms.CharField(widget=forms.PasswordInput(attrs={'class': 'form-control', 'placeholder': 'Neues Kennwort'}))
    new_password_again = forms.CharField(widget=forms.PasswordInput(attrs={'class': 'form-control', 'placeholder': 'Wiederholen Sie das neue Passwort'}))

    def __init__(self, user, *args, **kwargs):
        self.user = user
        super(UserPasswordChangeForm, self).__init__(*args, **kwargs)

    def clean(self):
        new_password = self.cleaned_data.get('new_password')
        new_password_again = self.cleaned_data.get('new_password_again')

        if new_password != new_password_again:
            self.add_error('new_password', 'Yeni Şifreler Eşleşmedi')
            self.add_error('new_password_again', 'Yeni Şifreler Eşleşmedi')

    def clean_old_password(self):
        old_password = self.cleaned_data.get('old_password')
        if not self.user.check_password(old_password):
            raise forms.ValidationError('Lütfen Şifrenizi Giriniz')
        return old_password


class ClientCompanyForm(forms.ModelForm):
    vergi_dairesi = forms.CharField(max_length=250, required=False, widget=forms.TextInput(attrs={'class': 'form-control','id':'vergi_dairesi', 'placeholder': 'Steuerverwaltung' }))
    vergi_no = forms.CharField(max_length=250, required=False, widget=forms.TextInput(attrs={'class': 'form-control','id':'vergi_no', 'placeholder': 'Steuernummer'}))
    firma_unvani = forms.CharField(max_length=250, required=False, widget=forms.TextInput(attrs={'class': 'form-control','id':'firma_unvani', 'placeholder': 'Name der Firma'}))
    company_address = forms.CharField(max_length=250, required=False, widget=forms.TextInput(attrs={'class': 'form-control','id':'company_address', 'placeholder': 'Firmenanschrift'}))
    company_telephone =forms.CharField(max_length=250, required=False, widget=forms.TextInput(attrs={'class': 'form-control','id':'company_telephone', 'placeholder': 'Firmennummer'}))
    
    class Meta:
        model = ClientCompany
        fields = ('vergi_dairesi','vergi_no','firma_unvani', 'company_address','company_telephone')


class ClientRegisterForm(forms.ModelForm):
    tckn = forms.CharField(max_length=250, widget=forms.TextInput(attrs={'class': 'form-control','id':'tckn', 'placeholder': 'Müşteri TC Kimlik No', 'onkeypress':'if ( isNaN( String.fromCharCode(event.keyCode) )) return false;' , 'maxLength':11, 'oninput':'javascript: if (this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);'}))
    class Meta:
        model = Client
        fields = ('tckn',)

class SuperadminRegisterForm(forms.ModelForm):
    superadmin_duty = forms.CharField(max_length=250, widget=forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'Mitarbeiterpflicht'}))
    class Meta:
        model = Superadmin
        fields = ('superadmin_duty',)

class DutyForm(forms.ModelForm):
    duty_name = forms.CharField(max_length=250, widget=forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'Aufgabe'}))
    class Meta:
        model = Duty
        fields = ('duty_name',)

class KullaniciRegisterForm(forms.ModelForm):
    duty = forms.ModelChoiceField(queryset=Duty.objects.all(),widget=forms.Select(attrs={'class':'selectpicker', 'data-live-search':'true'}))
    class Meta:
        model = Kullanici
        fields = ('duty',)


class CompanyInfoForm(forms.ModelForm):
    company_address = forms.CharField(max_length=400, widget=forms.Textarea(attrs={'rows':5,'class': 'form-control', 'placeholder': 'Firmenanschrift'}))
    company_address_2 = forms.CharField(max_length=400, widget=forms.Textarea(attrs={'rows':5,'class': 'form-control', 'placeholder': 'Firmenanschrift - 2'}))
    company_phone_number = forms.CharField(max_length=60, widget=forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'Firmennummer'}))
    company_phone_number_2 = forms.CharField(max_length=60, widget=forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'Firmennummer - 2'}))
    company_mobile_phone_number = forms.CharField(max_length=60, required=False, widget=forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'Handynummer des Unternehmens'}))
    company_mobile_phone_number_two = forms.CharField(max_length=60, required=False, widget=forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'Handynummer des Unternehmens 2'}))
    company_email = forms.EmailField(max_length=60, widget=forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'Email', 'type':'email'}))
    company_twitter = forms.CharField(max_length=60, required=False, widget=forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'Twitter-Adresse des Unternehmens'}))
    company_instagram = forms.CharField(max_length=60, required=False, widget=forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'Instagram-Adresse des Unternehmens'}))
    company_facebook = forms.CharField(max_length=60, required=False, widget=forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'Facebook-Adresse des Unternehmens'}))
    company_youtube = forms.CharField(max_length=60, required=False, widget=forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'Youtube'}))
    company_skype = forms.CharField(max_length=60, required=False, widget=forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'Skype'}))
    company_pinterest = forms.CharField(max_length=60, required=False, widget=forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'Pinterest'}))
    company_google = forms.CharField(max_length=60, required=False, widget=forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'Google'}))
    company_flickr = forms.CharField(max_length=60, required=False, widget=forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'Flickr'}))
    company_about_us = forms.CharField(max_length=400, widget=forms.Textarea(attrs={'rows':5,'class': 'form-control', 'placeholder': 'Firma über uns'}))

    class Meta:
        model = CompanyInfo
        fields = ('company_address', 'company_address_2','company_phone_number','company_phone_number_2', 'company_mobile_phone_number', 'company_mobile_phone_number_two', 'company_email', 'company_twitter', 'company_instagram', 'company_facebook','company_youtube','company_skype','company_pinterest','company_google','company_flickr', 'company_about_us')


class CompanyImageForm(forms.ModelForm):
    company_image = forms.ImageField(required=False,label='', widget=forms.ClearableFileInput(attrs={'class':'file-input'}))
    class Meta:
        model = CompanyImage
        fields = ('company_image',)
