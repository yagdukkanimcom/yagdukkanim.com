from django.shortcuts import render, redirect, get_object_or_404
from django.contrib.auth import authenticate, login, logout
from django.contrib import messages
from .forms import  LoginForm
from .models import CompanyInfo,CompanyImage

def get_auth(request):
    flag = False
    gidilecek_sayfa = ""
    if not request.user.is_authenticated:
        flag = True
        gidilecek_sayfa = 'accounts:login'
    try:
        musteri = request.user.client
        flag = True
        gidilecek_sayfa = 'website:index'
    except Exception as e:
        pass
    return flag, gidilecek_sayfa

def logout_view(request):
    logout(request)
    return redirect('accounts:login')

def login_view(request):
    print('fatih')
    if request.user.is_authenticated:
        try:
            superadmin = request.user.superadmin
            return redirect('adminpanel:index')
        except Exception as e:
            pass
        try:
            kullanici = request.user.kullanici
            return redirect('adminpanel:index')
        except Exception as e:
            pass
        try:
            musteri = request.user.client
            return redirect('website:index')
        except Exception as e:
            pass

    if request.POST:
        login_form = LoginForm(request.POST)
        if login_form.is_valid():
            username = request.POST['username']
            password = request.POST['password']
            print(username)
            print(password)

            user1 = authenticate(username=username, password=password)
            if user1:
                try:
                    superadmin = user1.superadmin
                    login(request, user1)
                    messages.success(request, 'Giriş İşlemi Başarılı')
                    return redirect('adminpanel:index')
                except Exception as e:
                    pass
                try:
                    kullanici = user1.kullanici
                    login(request, user1)
                    messages.success(request, 'Giriş İşlemi Başarılı')
                    return redirect('adminpanel:index')
                except Exception as e:
                    pass
                try:
                    client = user1.client
                    login(request, user1)
                    return redirect('musteri:musteri-ana-sayfa')
                except Exception as e:
                    pass
            else:
                messages.error(request, 'Giriş İşlemi Başarısız')
        else:
            messages.error(request, 'Giriş İşlemi Başarısız')
    else:
        login_form = LoginForm()
    try:
        company_info = CompanyInfo.objects.all()[0]
        company_image = CompanyImage.objects.all()[0]
    except Exception as e:
        company_info = ""
        company_image = ""
    context = {'login_form': login_form, 'company_info':company_info, 'company_image':company_image}
    return render(request, 'front_end/login/login.html', context)
