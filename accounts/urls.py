from django.urls import path
from . import views
from django.conf.urls import url
from .view import superadmin, client, kullanici

app_name = 'accounts'

urlpatterns = [
    path('logout/', views.logout_view, name="logout"),
    path('login/', views.login_view, name="login"),

    path('client-register/', client.client_register, name="client-register"),
    path('client-password-change/', client.client_password_change, name="client-password-change"),

    path('kullanici-register/', kullanici.kullanici_register, name="kullanici-register"),
    path('kullanici-update/', kullanici.kullanici_update, name="kullanici-update"),
    path('kullanici-password-change/', kullanici.kullanici_password_change, name="kullanici-password-change"),


    path('superadmin-register/', superadmin.superadmin_register, name="superadmin-register"),
    path('superadmin-update/', superadmin.superadmin_update, name="superadmin-update"),
    path('superadmin-password-change/', superadmin.superadmin_password_change, name="superadmin-password-change"),

    path('client-delete/<slug:slug>/', superadmin.client_delete, name="client-delete"),
    path('add-client/', superadmin.add_client, name="add-client"),
    path('client-password-change-from-admin/<slug:slug>/', superadmin.client_password_change_from_admin, name="client-password-change-from-admin"),
    path('client-update/<slug:slug>/', superadmin.client_update, name="client-update"),

    path('add-kullanici/', superadmin.add_kullanici, name="add-kullanici"),
    path('kullanici-list/', superadmin.kullanici_list, name="kullanici-list"),
    path('kullanici-delete/<slug:slug>/', superadmin.kullanici_delete, name="kullanici-delete"),
    path('kullanici-update-from-admin/<slug:slug>/', superadmin.kullanici_update_from_admin, name="kullanici-update-from-admin"),
    
    path('kullanici-duty-delete/<int:pk>/', superadmin.kullanici_duty_delete, name="kullanici-duty-delete"),
    path('kullanici-duty-update/<int:pk>/', superadmin.kullanici_duty_update, name="kullanici-duty-update"),
    path('add-list-duty/', superadmin.add_list_duty, name="add-list-duty"),

]
