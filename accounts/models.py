from django.db import models
from django.contrib.auth.models import User
from PIL import Image
from io import BytesIO
from django.template.defaultfilters import slugify
from django.core.files.uploadedfile import InMemoryUploadedFile
from django.shortcuts import reverse
import sys
from unidecode import unidecode
from datetime import datetime
from ckeditor.fields import RichTextField


class Duty(models.Model):
    duty_name = models.CharField(max_length=250, blank=True, null=True)

    def __str__(self):
        return self.duty_name

    class Meta:
        verbose_name_plural = "Kullanıcı Görevi"

class Kullanici(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    duty = models.ForeignKey(Duty, related_name='kullaniciduty',on_delete=models.CASCADE, blank=True, null=True)
    slug = models.SlugField(null=True, unique=True, editable=False)

    def __str__(self):
        return self.user.email

    class Meta:
        verbose_name_plural = "Kullanıcı"

    def get_unique_slug(self):
        sayi=0
        slug = slugify(unidecode(self.user.username))
        new_slug=slug
        while Kullanici.objects.filter(slug=new_slug).exists():
            sayi+=1
            new_slug="%s-%s"%(slug,sayi)
        slug = new_slug
        return slug

    def save(self, *args, **kwargs):
        if self.id is None:
            kullanici_username = self.get_unique_slug()
            self.slug = slugify(unidecode(kullanici_username))
        else:
            kullanici=Kullanici.objects.get(slug=self.slug)
            if kullanici.user.username != self.user.username:
                self.slug=self.get_unique_slug()
        super(Kullanici,self).save()

class Superadmin(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    superadmin_duty = models.CharField(max_length=250, blank=True, null=True)

    def __str__(self):
        return self.user.email

    class Meta:
        verbose_name_plural = "Superadmin"

class CompanyInfo(models.Model):
    company_name = models.CharField(max_length=150, null=True)
    company_about_us = RichTextField(max_length=1000, verbose_name='Hakkımızda', blank="True", null="True")
    company_vision = RichTextField(max_length=1000, verbose_name='Vizyon', blank="True", null="True")
    company_mission = RichTextField(max_length=1000, verbose_name='Misyon', blank="True", null="True")
    company_address = RichTextField(max_length=1000, verbose_name='Adres', blank="True", null="True")
    company_address_2 = RichTextField(max_length=1000, verbose_name='Adres 2', blank="True", null="True")
    company_phone_number = models.CharField(max_length=50, null=True)
    company_phone_number_2 = models.CharField(max_length=50, null=True, blank = True)
    company_mobile_phone_number = models.CharField(max_length=50, null=True, blank = True)
    company_mobile_phone_number_two = models.CharField(max_length=50, null=True, blank = True)
    company_email = models.EmailField(max_length=150, null=True)
    company_twitter = models.CharField(max_length=150, null=True, blank = True)
    company_instagram = models.CharField(max_length=150, null=True, blank = True)
    company_facebook = models.CharField(max_length=150, null=True, blank = True)
    company_youtube = models.CharField(max_length=150, null=True, blank = True)
    company_skype = models.CharField(max_length=150, null=True, blank = True)
    company_pinterest = models.CharField(max_length=150, null=True, blank = True)
    company_flickr = models.CharField(max_length=150, null=True, blank = True)
    company_google = models.CharField(max_length=150, null=True, blank = True)

    class Meta:
        verbose_name_plural = "Firma Bilgileri"

    def __str__(self):
        return str(self.company_name)

class CompanyImage(models.Model):
    company_image = models.ImageField(upload_to='company',verbose_name='Image', null=True, blank=True)

    def get_image_url(self):
        return self.company_image.url

    def save(self, *args, **kwargs):
        imageTemproary = Image.open(self.company_image)
        #imageTemproary1 = imageTemproary.convert('RGB')
        outputIoStream = BytesIO()
        w, h = imageTemproary.size
        imageTemproaryResized = imageTemproary.resize( (int(w/1), int(h/1))  )
        imageTemproaryResized.save(outputIoStream , format='PNG', quality=85)
        outputIoStream.seek(0)
        self.company_image = InMemoryUploadedFile(outputIoStream,'ImageField', "%s.png" %self.company_image.name.split('.')[0], 'image/png', sys.getsizeof(outputIoStream), None)
        super(CompanyImage, self).save(*args, **kwargs)

    class Meta:
        verbose_name_plural = "Firma Fotoğrafı"