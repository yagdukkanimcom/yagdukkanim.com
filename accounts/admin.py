from django.contrib import admin
from .models import CompanyInfo, CompanyImage, Superadmin, Kullanici, Duty 

admin.site.register(CompanyInfo)
admin.site.register(CompanyImage)
admin.site.register(Superadmin)
admin.site.register(Kullanici)
admin.site.register(Duty)