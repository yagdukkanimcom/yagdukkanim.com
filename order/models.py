from django.db import models
from django.contrib.auth.models import User 
from product.models import Product, ProductStock
from client.models import Client , Address


class ReceivedProduct(models.Model):

    STATUS = [
        ('Neu','Neu'),
        ('Genehmigt','Genehmigt'),
        ('Vorbereiten','Vorbereiten'),
        ('In Cargo','In Cargo'),
        ('Abgeschlossen','Abgeschlossen'),
        ('Stornierung','Stornierung'),
    ]

    fname = models.CharField(max_length=150,blank=True, null=True)
    lname = models.CharField(max_length=150,blank=True, null=True)
    email = models.CharField(max_length=150,blank=True, null=True)
    tel   = models.CharField(max_length=50, blank=True, null=True)
    siparis_adresi_2   = models.CharField(max_length=550, blank=True, null=True)
    product_recieved_size   = models.CharField(max_length=50, blank=True, null=True)

    creation_date = models.DateField(auto_now=True, blank=True, null=True)
    client = models.ForeignKey(Client, verbose_name="clientsiparis", on_delete=models.SET_NULL, blank=True, null=True)
    product = models.ForeignKey(Product, on_delete=models.SET_NULL , null= True) # cascade yapılacak HATA olursa
    quantity = models.IntegerField()
    siparis_adresi = models.ForeignKey(Address, verbose_name="siparisadresi", on_delete=models.SET_NULL, blank=True, null=True)
    firma_mesaji = models.CharField(max_length=15, choices=STATUS, default ='Neu')
    payment_method = models.CharField(max_length=50, blank=True, null=True)

    
    def __str__(self):
        if self.client:
            return str(self.client)
        else:
            return str(self.product)

    @property
    def amount(self):
        if self.product == None:
            return ""
        if self.product.product_sale == "Indirimli Urun":
            return (self.quantity * self.product.product_price_sale)
        return (self.quantity * self.product.product_price)

    @property
    def price(self):
        if self.product == None:
            return ""
        if self.product.product_sale == "Indirimli Urun":
            return self.product.product_price_sale
        return self.product.product_price

    # @property
    # def amount2(self):
    #     if self.product == None:
    #         return ""
    #     if self.product.product_sale == "Indirimli Urun":
    #         return (self.quantity * self.product.product_price2_sale)
    #     return (self.quantity * self.product.product_price2)

    # @property
    # def price2(self):
    #     if self.product == None:
    #         return ""
    #     if self.product.product_sale == "Indirimli Urun":
    #         return self.product.product_price2_sale
    #     return self.product.product_price2


    class Meta:
        verbose_name_plural = "Siparişler"

class ShopCart (models.Model):
    user = models.ForeignKey(User, on_delete=models.SET_NULL , null= True)
    product = models.ForeignKey(Product, on_delete=models.SET_NULL , null= True) # cascade yapılacak HATA olursa
    quantity = models.IntegerField()
    session_id = models.CharField(max_length=50, null= True, blank=True)
    payment_type = models.CharField(max_length=90, null= True, blank=True)

    def __str__(self):
        if self.product == None:
            return ""
        if self.user:
            return self.product.product_name + " - " + self.user.username
        else:
            return self.product.product_name + " - " + self.session_id

    class Meta:
        verbose_name_plural = "Sepet"

    @property
    def amount(self):
        if self.product == None:
            return ""
        return (self.quantity * self.product.product_price_end)

    @property
    def kdv_amount(self):
        if self.product == None:
            return ""
        return format(self.quantity * float(self.product.product_KDV_rate / 100) * float(self.product.product_discount_price) , '.2f') 

    @property
    def discount_amount(self):
        if self.product == None: 
            return ""
        return format(float(self.product.product_discount_rate / 100) * float(self.product.product_price) , '.2f') 


    @property
    def price(self):
        if self.product == None:
            return ""
        return self.product.product_price_end

    # @property
    # def amount2(self):
    #     if self.product == None:
    #         return ""
    #     if self.product.product_sale == "Indirimli Urun":
    #         return (self.quantity * self.product.product_price2_sale)
    #     return (self.quantity * self.product.product_price2)

    # @property
    # def price2(self):
    #     if self.product == None:
    #         return ""
    #     if self.product.product_sale == "Indirimli Urun":
    #         return self.product.product_price2_sale
    #     return self.product.product_price2


class Order (models.Model):

    STATUS = [
        ('Neu','Neu'),
        ('Genehmigt','Genehmigt'),
        ('Vorbereiten','Vorbereiten'),
        ('In Cargo','In Cargo'),
        ('Abgeschlossen','Abgeschlossen'),
        ('Stornierung','Stornierung'),
    ]

    COUNTRY = (

        ('Türkiye','Türkiye'),
        ('Almanya','Almanya'),
        ('Hollanda','Hollanda'),
        ('Amerika','Amerika'),
        ('İngiltere','İngiltere'),
        ('Rusya','Rusya'),
    )
    user =  models.ForeignKey(User, on_delete=models.SET_NULL , null= True)
    code = models.CharField(max_length=5, editable=False)
    first_name = models.CharField(max_length=50)
    last_name = models.CharField(max_length=50)
    phone = models.CharField(blank=True,max_length=20 , null= True)
    adress = models.CharField(blank=True, max_length=150)
    city = models.CharField(blank=True, max_length=50)
    country = models.CharField(blank=True, max_length=50, choices=COUNTRY)
    total = models.FloatField()
    status = models.CharField(max_length=15, choices=STATUS, default ='Neu')
    ip = models.CharField(blank=True,max_length=20)
    adminnote = models.CharField(blank=True,max_length=100)
    create_at=models.DateField(auto_now_add=True)
    update_at=models.DateField(auto_now=True)

    def __str__(self):
        return self.first_name

class OrderProduct (models.Model):
    STATUS = (

        ('Yeni','Yeni'),
        ('Onaylandı','Onaylandı'),
        ('İptal','İptal'),
    )

    order =  models.ForeignKey(Order, on_delete=models.CASCADE, null=True)
    user =  models.ForeignKey(User, on_delete=models.CASCADE, null=True)
    product = models.ForeignKey(Product , on_delete=models.CASCADE)
    quantity = models.IntegerField()
    price = models.FloatField()
    amount = models.FloatField()
    status = models.CharField(max_length=15, choices=STATUS, default ='Neu')
    create_at=models.DateField(auto_now_add=True)
    update_at=models.DateField(auto_now=True)

    def __str__(self):
        return self.product.product_name

class IadeTalebi (models.Model):

    STATUS = (
        ('Onay Bekliyor','Onay Bekliyor'),
        ('Onaylandı','Onaylandı'),
        ('Tamamlandı','Tamamlandı'),
        ('İptal','İptal'),
    )

    client = models.ForeignKey(Client, verbose_name="clientsiparis", on_delete=models.SET_NULL, blank=True, null=True)
    siparis =models.ForeignKey(ReceivedProduct, related_name='iadesiparis',on_delete=models.SET_NULL, blank=True, null=True)
    firma_mesaji = models.CharField(max_length=1500, choices=STATUS, default ='Firma Mesajı')

    def __str__(self):
        return self.firma_mesaji

    class Meta:
        verbose_name_plural = "İade Talebi"

class PaymentTypeModel (models.Model):
    user = models.ForeignKey(User, on_delete=models.SET_NULL , null= True)
    payment_type = models.CharField(max_length=150, null= True, blank=True)
    session_id = models.CharField(max_length=150, null= True, blank=True)

    def __str__(self):
        return self.payment_type

    class Meta:
        verbose_name_plural = "Ödeme Tipi"

