from django import forms
from .models import Order,ShopCart , ReceivedProduct , IadeTalebi

class OrderForm(forms.ModelForm):

    class Meta:
        model = Order
        fields = ['first_name','last_name','adress','phone','city','country']


class ShopCartForm(forms.ModelForm):
    
    class Meta:
       model = ShopCart
       fields = ['quantity']



STATUS_CHOICES = [
    ('Neu','Neu'),
    ('Genehmigt','Genehmigt'),
    ('Vorbereiten','Vorbereiten'),
    ('In Cargo','In Cargo'),
    ('Abgeschlossen','Abgeschlossen'),
    ('Stornierung','Stornierung'),
]

class SiparisDuzenlemeFormu(forms.ModelForm):
    firma_mesaji =forms.ChoiceField(
        widget=forms.RadioSelect,
        choices = STATUS_CHOICES,
        required=False,
    )
    class Meta:
        model = ReceivedProduct
        fields = ['firma_mesaji']

IADE_STATUS_CHOICES = [
    ('Ausstehend','Ausstehend'),
    ('Genehmigt','Genehmigt'),
    ('Abgeschlossen','Abgeschlossen'),
    ('Stornierung','Stornierung'),
]

class IadeTalebiDuzenlemeFormu(forms.ModelForm):
    firma_mesaji =forms.ChoiceField(
        widget=forms.RadioSelect,
        choices = IADE_STATUS_CHOICES,
        required=False,
    )
    class Meta:
        model = IadeTalebi
        fields = ['firma_mesaji']