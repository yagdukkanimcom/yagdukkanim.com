from django.contrib import admin
from .models import Order,OrderProduct,ShopCart,ReceivedProduct, IadeTalebi,PaymentTypeModel
# Register your models here.

# class ShopCartAdmin(admin.ModelAdmin):
#     list_display = ['user','product','price','quantity','amount', 'session_id']

admin.site.register(Order)
admin.site.register(OrderProduct)
# admin.site.register(ShopCart,ShopCartAdmin)
admin.site.register(ShopCart)
admin.site.register(IadeTalebi)

admin.site.register(ReceivedProduct)
admin.site.register(PaymentTypeModel)
