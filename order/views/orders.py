from django.shortcuts import render, get_object_or_404,redirect,HttpResponseRedirect,HttpResponse
from product.models import Product ,ProductStock, Brand
from ..models import ShopCart 
from ..forms import ShopCartForm 
from django.contrib import messages
from django.utils.crypto import get_random_string
from client.models import *
from category.models import Category

def delete_cart_product(request, pk):
    url = request.META.get('HTTP_REFERER')
    shop_cart = get_object_or_404(ShopCart, pk=pk)
    shop_cart.delete()

    # current_user=request.user
    # request.session['cart_items'] = ShopCart.objects.filter(user_id=current_user.id).count()

    return HttpResponseRedirect(url)

def add_to_cart(request, pk):
    url = request.META.get('HTTP_REFERER') # get last url 
    session_id = request.session.session_key  # request.COOKIES['sessionid']
    current_user=request.user

    if not request.session.exists(request.session.session_key):
        request.session.create() 
        session_id = request.session.session_key
    product = get_object_or_404(Product, pk=pk)

    try:
        checkproducts=ShopCart.objects.filter(product=product, user=current_user) #ürün sepette var mı sorgusu
    except Exception as ex:
        checkproducts=ShopCart.objects.filter(product=product, session_id=session_id)

    if request.method =='POST': # form post edildiyse, detay sayfasından geldiyse
        form = ShopCartForm(request.POST)

        ######********* Ürün sepette var mı kontrol *****************

        if form.is_valid():
            if checkproducts:  #ürün sepette varsa güncelle
                try:
                    data = ShopCart.objects.get(product=product, user=current_user)
                except Exception as ex:
                    data = ShopCart.objects.get(product=product, session_id=session_id)
                
                data.quantity += form.cleaned_data['quantity']
                data.save()
            else:
                try:
                    data = ShopCart.objects.create(user=current_user, product=product, quantity = form.cleaned_data['quantity'], session_id=session_id)
                except Exception as ex:
                    data = ShopCart.objects.create(product=product, quantity = form.cleaned_data['quantity'], session_id=session_id)
                data.save()  #veri tabanına kaydet
        
        request.session['cart_items'] = ShopCart.objects.filter(product=product, session_id=session_id).count()
        messages.success(request, 'Ürün Sepete Eklendi')

        return HttpResponseRedirect(url)

    else:  # Ürün direkt sepete eklendiyse örneğin ana sayfadan butondan
        
        if checkproducts:  #ürün sepette varsa güncelle
            try:
                data = ShopCart.objects.get(product=product, user=current_user)

            except Exception as ex:
                data = ShopCart.objects.get(product=product, session_id=session_id)
            data.quantity += 1
            data.save()
        else:
            try:
                data = ShopCart.objects.create(user=current_user, product=product, quantity = 1, session_id=session_id)
            except Exception as ex:
                data = ShopCart.objects.create(product=product, quantity = 1, session_id=session_id)
            try:
                data.user_id = current_user.id
            except Exception as ex:
                data.session_id = session_id
            data.product_id = pk
            data.save()
    
        request.session['cart_items'] = ShopCart.objects.filter(session_id=session_id).count()
        messages.success(request, 'Ürün Sepete Eklendi')
        return HttpResponseRedirect(url)
            
    return HttpResponseRedirect(url)


def update_cart(request, pk):

    url = request.META.get('HTTP_REFERER') 
    session_id = request.session.session_key 
    current_user=request.user

    if not request.session.exists(request.session.session_key):
        request.session.create() 
        session_id = request.session.session_key

    if request.method =='POST': 
        quantity = request.POST.get("quantity")
        try:
            data = ShopCart.objects.get(pk=pk, user=current_user)
        except Exception as ex:
            data = ShopCart.objects.get(pk=pk, session_id=session_id)
        data.quantity = quantity
        data.save()

        request.session['cart_items'] = ShopCart.objects.filter(pk=pk, session_id=session_id).count()
        messages.success(request, 'Ürün Sepete Eklendi')

        return HttpResponseRedirect(url)
    return HttpResponseRedirect(url)
    

# def orderproduct(request):
#     category = Category.objects.all().order_by('category_name')
#     current_user=request.user
#     session_id = request.session.session_key  # request.COOKIES['sessionid']
#     if not request.session.exists(request.session.session_key):
#         request.session.create() 
#         session_id = request.session.session_key
#     is_it_user = "False"
#     try:
#         shop_cart = ShopCart.objects.filter(user=current_user)
#         is_it_user= 'True'
#     except Exception as ex:
#         shop_cart = ShopCart.objects.filter(session_id=session_id)
    
#     total=0.0
#     # for rs in shop_cart:
#     #     if rs.product.product_sale == "Indirimli Urun":
#     #         total +=rs.product.product_price_sale * rs.quantity
#     #     else:
#     #         total +=rs.product.product_price * rs.quantity

#     total2=0.0
#     # for rs in shop_cart:
#     #     if rs.product.product_sale == "Indirimli Urun":
#     #         total2 +=rs.product.product_price2_sale * rs.quantity
#     #     else:
#     #         total2 +=(rs.product.product_price2) * rs.quantity

         
#     """
#         if request.method =='POST': # form post edildiyse, detay sayfasından geldiyse
#             form = OrderForm(request.POST)

#             if form.is_valid():
#                 # KREDİ KARTI BİLGİLERİNİ BANKAYA GÖNDER ONAY GELİRSE DEVAM ET 
#                 # .........
#                 data = Order()
#                 data.first_name = form.cleaned_data['first_name']
#                 data.last_name = form.cleaned_data['last_name']
#                 data.address = form.cleaned_data['address']
#                 data.city = form.cleaned_data['city']
#                 data.phone = form.cleaned_data['phone']
#                 try:
#                     data.user_id = current_user.id
#                 except Exception as ex:
#                     pass
                
#                 data.total = total
#                 data.ip = request.META.get('REMOTE_ADDR')
#                 ordercore = get_random_string(5).upper()
#                 date.order = ordercore
#                 data.save()
                

#                 try:
#                     shop_cart = ShopCart.objects.filter(user=current_user)
#                 except Exception as ex:
#                     shop_cart = ShopCart.objects.filter(session_id=session_id)

#                 for rs in shop_cart:
#                     detail = OrderProduct()
#                     detail.order_id       =data.id # order id
#                     detail.product_id     =rs.product_id
#                     detail.user_id        =current_user.id
#                     detail.quantity       =rs.quantity
#                     detail.price          =rs.product.price 
#                     detail.amount         =rs.amount
#                     detail.save()

#                     product            = Product.objects.get(id=rs.product_id)
#                     product.stock    -= rs.quantity
#                     product.save()
#                 try:
#                     ShopCart.objects.filter(user_id=current_user.id).delete()
#                 except Exception as ex:
#                     ShopCart.objects.filter(session_id=session_id).delete()
                
#                 request.session['cart_items'] = 0
#                 messages.success(request, 'Order işlemi Başarılı')

#                 context = {'ordercore':ordercore, 'category':category,'is_it_user':is_it_user}
#                 return render(request,'website/checkout2.html', context)

#             else:
#                 messages.warning(request, form.errors)
#                 return HttpResponseRedirect("/order/odeme") 

#         form = OrderForm()
#         #profile=UserProfile.objects.get(user_id=current_user.id)
#     """
    
#     try:
#         company=ClientCompany.objects.filter(client=request.user.client)
#         company_default = company[0]
#     except Exception as ex:
#         company = ""
#         company_default=""

#     try:
#         address = Address.objects.filter(client=request.user.client)
#         address_default = address[0]
#     except Exception as ex:
#         address_default = ""
#         address = ""



#     products = Product.objects.all()

#     context = {
#         'shop_cart':shop_cart,'total':total, 'total2':total2, 
#         'products':products,'address':address, "address_default":address_default,
#         'company':company, 'is_it_user':is_it_user,
#         'company_default':company_default,
    
#     }
#     return render(request,'website/checkout2.html', context)



# def cart_view(request):
#     products = Product.objects.all()

#     try:
#         size_name = ProductStock.objects.filter()
#         stock = ProductStock.objects.filter(product=shop_cart.product)

#     except Exception as ex:
#         if request.session['cart_items'] == 0:
#             sc = ShopCart.objects.filter(user=None)
#             for i in sc:
#                 sc.delete()

#     stock ="l"    

#     #request.session['cart_items'] = ShopCart.objects.filter(user_id=current_user.id).count()
#     context = {'stock':stock, 'products':products}
#     return render(request,'website/cart.html', context)


# def stripe_view(request):
#     context = { }
#     return render(request, "home.html", context)
