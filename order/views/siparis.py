from django.shortcuts import render, get_object_or_404,redirect,HttpResponseRedirect,HttpResponse
from ..models import ReceivedProduct
from ..forms import SiparisDuzenlemeFormu , IadeTalebiDuzenlemeFormu

from django.contrib import messages
from client.models import Address  
from accounts.views import get_auth


def siparis_listele_view(request):
    flag, gidilecek_sayfa = get_auth(request)
    if flag:
        return redirect(gidilecek_sayfa)
    
    siparisler = ReceivedProduct.objects.all()

    context = { 'siparisler':siparisler ,'pageheader': 'Siparişler'}

    return render(request, 'adminpanel/siparis/siparis_listele.html', context)
  
def siparis_duzenle_view(request, pk):
    flag, gidilecek_sayfa = get_auth(request)
    if flag:
        return redirect(gidilecek_sayfa)
    
    siparis = get_object_or_404(ReceivedProduct, pk=pk)
    
    form = SiparisDuzenlemeFormu(instance=siparis, data=request.POST or None)
    if request.POST:
        if form.is_valid():
            form.save()
            messages.success(request, 'Güncelleme İşlemi Başarılı')
            return redirect('order:siparis-listele')
        else:
            messages.error(request, 'Güncelleme İşlemi Başarısız')
    
    try:
        address = Address.objects.filter(client=siparis.client)
    except Exception as identifier:
        address = ""

    context = {'siparis':siparis, 'form': form, 'pageheader': 'Siparis Güncelle', 'address':address}

    return render(request, 'adminpanel/siparis/siparis_duzenle.html',context)

def siparis_sil_view(request, pk):
    flag, gidilecek_sayfa = get_auth(request)
    if flag:
        return redirect(gidilecek_sayfa)
    
    siparis = get_object_or_404(ReceivedProduct, pk=pk)
    siparis.delete()
    messages.success(request, 'Silme İşlemi Başarılı')

    return redirect('order:siparis-listele')


