from django.urls import path
from .views import orders, siparis

app_name = 'order'

urlpatterns = [
    path('addtocart/<int:pk>/', orders.add_to_cart, name='addtocart'),
    path('delete-cartproduct/<int:pk>/', orders.delete_cart_product, name='delete-cartproduct'),
    path('updatecart/<int:pk>/', orders.update_cart, name='update-cart'),
    
    # path('odeme/', orders.orderproduct, name='checkout'),
    # path('sepet/', orders.cart_view, name="cart"),
    # path('stripe-odeme/', orders.stripe_view, name="stripe-odeme"),


    path('siparis-duzenle/<int:pk>/', siparis.siparis_duzenle_view, name="siparis-duzenle"),
    path('siparis-listele/', siparis.siparis_listele_view, name="siparis-listele"),
    path('siparis-sil/<int:pk>/', siparis.siparis_sil_view, name="siparis-sil"),
    
]