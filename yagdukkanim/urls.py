from django.contrib import admin
from django.urls import path,include

from django.conf.urls.static import static
from django.conf import settings

urlpatterns = [
    path('admin/', admin.site.urls),
    path('',include(('website.urls','website'),namespace='website')),
    path('adminpanel/', include(('adminpanel.urls','adminpanel'),namespace='adminpanel')),
    path('hesap/', include(('accounts.urls','accounts'), namespace='accounts')),
    path('satis/', include(('order.urls','order'), namespace='order')),
    path('kategori/', include(('category.urls','category'), namespace='category')),
    path('urun/', include(('product.urls','product'), namespace='product')),
    path('musteri/', include(('client.urls','client'), namespace='client')),

]

urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
