from category.models import Category
from website.views import category
from order.models import ShopCart, PaymentTypeModel
from product.models import Product, Brand
from datetime import datetime, timedelta
import math
from accounts.models import  CompanyInfo, CompanyImage

def get_company(request):
    try:
        company_info = CompanyInfo.objects.all()[0]
        company_image = CompanyImage.objects.all()[0]
    except Exception as e:
        company_info = ""
        company_image = ""

    return_company = { 'company_image':company_image, 'company_info':company_info }

    return return_company

def total_index(request):
    current_user=request.user
    session_id = request.session.session_key

    product_special = Product.objects.filter(product_special=True)
    product_popular = Product.objects.filter(product_popular=True)
    product_new = Product.objects.filter(creation_date__gt = datetime.now() - timedelta(days=20))[:10]

    categories = Category.objects.all().order_by('category_name')
    brands =  Brand.objects.all().order_by('brand_name')

    if not request.session.exists(request.session.session_key):
        request.session.create() 
        session_id = request.session.session_key
    
    shop_cart = ""
    if str(request.user) != "AnonymousUser":
        shop_cart = ShopCart.objects.filter(user=current_user)
    elif session_id:
        shop_cart = ShopCart.objects.filter(session_id=session_id)

    total = 0
    total2 = 0
    total_kdv = 0
    total_discount = 0

    for rs in shop_cart:
        total += float(rs.amount)
        total2 += float(rs.product.product_price) * rs.quantity
        total_kdv += float(rs.kdv_amount)
        total_discount += float(rs.discount_amount)  * rs.quantity



    return_list = { 'shop_cart': shop_cart, 'total': total, 'total2':total2, 'total_kdv':total_kdv, 'total_discount':total_discount,
                    'product_new':product_new, 'product_special':product_special, 'product_popular':product_popular,
                    'categories':categories, 'brands':brands
                     }

    return return_list




# def total_index(request):
    current_user=request.user
    session_id = request.session.session_key

    product_special = Product.objects.filter(product_special=True)
    product_popular = Product.objects.filter(product_popular=True)
    product_new = Product.objects.filter(creation_date__gt = datetime.now() - timedelta(days=20))[:10]

    categories = Category.objects.all().order_by('category_name')
    brands =  Brand.objects.all().order_by('brand_name')

    if not request.session.exists(request.session.session_key):
        request.session.create() 
        session_id = request.session.session_key
    
    
    shop_cart = ""
    if str(request.user) != "AnonymousUser":
        shop_cart = ShopCart.objects.filter(user=current_user)
        cart_item_count = ShopCart.objects.filter(user_id=current_user.id).count()
    elif session_id:
        shop_cart = ShopCart.objects.filter(session_id=session_id)
        cart_item_count = ShopCart.objects.filter(session_id=session_id).count()

    total = 0
    for rs in shop_cart:
        total += float(rs.amount)

    total2 = 0
    # for rs in shop_cart:
    #     total2 += float(rs.amount2)


    total3 = 0
    total4 = 0
    try:
        try:
            #payment_type_obj = get_object_or_404(PaymentTypeModel, user=current_user)
            payment_type_obj = PaymentTypeModel.objects.filter(user=current_user).order_by('-id')[0]
        except Exception as ex:
            #payment_type_obj = get_object_or_404(PaymentTypeModel,session_id=session_id2)
            payment_type_obj = PaymentTypeModel.objects.filter(session_id=session_id).order_by('-id')[0]

        if payment_type_obj.payment_type == "ZAHLEN SIE IN EUR.":
            if payment_type_obj.address_country == "Switzerland" or "Liechtenstein":
                total3 = int(math.ceil(total2*0.077) + total2 )
                if total3 < 100:
                    total3 = int(total3 + 10)
            else:
                total3 = int(math.ceil(total2*0.077) + total2 )
                if total3 < 100:
                    total3 = int(total3 + 15)


        else:
            if payment_type_obj.address_country == "Switzerland" or "Liechtenstein":
                total4 = int(math.ceil(total*0.077) + total )
                if total4 < 100:
                    total4 = int(total4 + 10 )
            else:
                total4 = int(math.ceil(total*0.077) + total )
                if total4 < 100:
                    total4 = int(total4 + 15)
    except Exception as ex:
        payment_type_obj = None

    return_list = { 'shop_cart': shop_cart, 'total': total, 'total2':total2, 'cart_item_count': cart_item_count,
                    'product_new':product_new, 'product_special':product_special, 'product_popular':product_popular,
                    'payment_type_obj':payment_type_obj, 'total3':total3, 'total4':total4, 'categories':categories, 'brands':brands
                     }

    return return_list