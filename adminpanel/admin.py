from django.contrib import admin
from .models import Blog, Mesajlar, SliderImage , SSS, Footer, Sayfalar

admin.site.register(Blog)
admin.site.register(Mesajlar)
admin.site.register(SliderImage)
admin.site.register(SSS)
admin.site.register(Footer)
admin.site.register(Sayfalar)
