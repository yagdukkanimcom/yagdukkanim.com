from django.shortcuts import render, redirect, get_object_or_404
from django.contrib import messages
from accounts.views import get_auth
from .models import  Mesajlar
from django.contrib.auth import authenticate
from django.contrib.auth import update_session_auth_hash


def index(request):
    """
    flag, gidilecek_sayfa = get_auth(request)
    if flag:
        return redirect(gidilecek_sayfa)
    """

    mesajlar =Mesajlar.objects.all()



    context = { 'mesajlar':mesajlar }
    return render(request,'adminpanel/index/index.html', context)
