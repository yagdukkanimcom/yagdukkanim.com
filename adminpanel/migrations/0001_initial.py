# Generated by Django 3.1.7 on 2021-07-09 10:47

import ckeditor.fields
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Ambiente',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('title', models.CharField(blank=True, max_length=150, null=True)),
                ('context', ckeditor.fields.RichTextField(blank='False', max_length=3000, null='True', verbose_name='İçerik Giriniz')),
                ('slug', models.SlugField(editable=False, null=True, unique=True)),
                ('creationDate', models.DateField(auto_now=True, null=True)),
                ('description', models.CharField(blank=True, max_length=350, null=True)),
            ],
            options={
                'verbose_name_plural': 'Ambiente',
            },
        ),
        migrations.CreateModel(
            name='Blog',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('title', models.CharField(blank=True, max_length=150, null=True)),
                ('context', ckeditor.fields.RichTextField(blank='False', max_length=3000, null='True', verbose_name='İçerik Giriniz')),
                ('slug', models.SlugField(editable=False, null=True, unique=True)),
                ('creationDate', models.DateField(auto_now=True, null=True)),
                ('description', models.CharField(blank=True, max_length=350, null=True)),
            ],
            options={
                'verbose_name_plural': 'Blog',
            },
        ),
        migrations.CreateModel(
            name='Footer',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('title', models.CharField(blank=True, max_length=250, null=True)),
                ('slug', models.SlugField(editable=False, null=True, unique=True)),
                ('link', models.CharField(blank=True, max_length=750, null=True)),
            ],
            options={
                'verbose_name_plural': 'Footer',
            },
        ),
        migrations.CreateModel(
            name='Home3',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('title', models.CharField(blank=True, max_length=150, null=True)),
                ('context', ckeditor.fields.RichTextField(blank='False', max_length=3000, null='True', verbose_name='İçerik Giriniz')),
                ('slug', models.SlugField(editable=False, null=True, unique=True)),
                ('creationDate', models.DateField(auto_now=True, null=True)),
                ('description', models.CharField(blank=True, max_length=350, null=True)),
            ],
            options={
                'verbose_name_plural': 'Home3',
            },
        ),
        migrations.CreateModel(
            name='Mesajlar',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('senders_name', models.CharField(blank=True, max_length=50, null=True)),
                ('senders_mail', models.EmailField(max_length=50, null=True)),
                ('tel', models.CharField(blank=True, max_length=50, null=True)),
                ('subject', models.CharField(max_length=50, null=True)),
                ('text', models.CharField(max_length=500, null=True)),
                ('creationDate', models.DateField(auto_now=True, null=True)),
            ],
            options={
                'verbose_name_plural': 'Müşteri Mesajları',
            },
        ),
        migrations.CreateModel(
            name='Sayfalar',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('zollfrei_shopping', ckeditor.fields.RichTextField(blank='False', max_length=14000, null='True', verbose_name='Zollfrei Shopping')),
                ('zhlungsmethoden', ckeditor.fields.RichTextField(blank='False', max_length=14000, null='True', verbose_name=' Zahlungsmethoden')),
                ('agbs', ckeditor.fields.RichTextField(blank='False', max_length=14000, null='True', verbose_name='AGBS')),
                ('impressum', ckeditor.fields.RichTextField(blank='False', max_length=14000, null='True', verbose_name='Impressum')),
                ('datenschutzerklarung', ckeditor.fields.RichTextField(blank='False', max_length=14000, null='True', verbose_name='Datenschutzerklärung')),
            ],
            options={
                'verbose_name_plural': 'Sayfalar',
            },
        ),
        migrations.CreateModel(
            name='Slider',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('title', models.CharField(blank=True, max_length=150, null=True)),
                ('context', ckeditor.fields.RichTextField(blank='False', max_length=3000, null='True', verbose_name='Slider İçeriği Giriniz')),
                ('slug', models.SlugField(editable=False, null=True, unique=True)),
                ('creationDate', models.DateField(auto_now=True, null=True)),
                ('link', models.CharField(blank=True, max_length=750, null=True)),
            ],
            options={
                'verbose_name_plural': 'Slider',
            },
        ),
        migrations.CreateModel(
            name='SSS',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('sss_title', models.CharField(blank=True, max_length=550, null=True)),
                ('sss_detail', ckeditor.fields.RichTextField(blank='False', max_length=5500, null='True', verbose_name='SSS İçeriğini Giriniz')),
                ('creationDate', models.DateField(auto_now=True, null=True)),
                ('sss_id', models.IntegerField(blank=True, null=True)),
            ],
            options={
                'verbose_name_plural': 'SSS',
            },
        ),
        migrations.CreateModel(
            name='SliderImage',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('slider_image', models.ImageField(blank=True, null=True, upload_to='slider', verbose_name='Image')),
                ('slider', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='slider_image', to='adminpanel.slider')),
            ],
            options={
                'verbose_name_plural': 'Slider Fotoğrafı',
            },
        ),
        migrations.CreateModel(
            name='Home3Image',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('home_3_image', models.ImageField(blank=True, null=True, upload_to='home_3', verbose_name='Image')),
                ('home_3', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='home_3_image', to='adminpanel.home3')),
            ],
            options={
                'verbose_name_plural': 'Home3 Fotoğrafı',
            },
        ),
        migrations.CreateModel(
            name='FooterImage',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('footer_image', models.ImageField(blank=True, null=True, upload_to='footer', verbose_name='Image')),
                ('footer', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='footer_image', to='adminpanel.footer')),
            ],
            options={
                'verbose_name_plural': 'Footer Fotoğrafı',
            },
        ),
        migrations.CreateModel(
            name='BlogImage',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('blog_image', models.ImageField(blank=True, null=True, upload_to='blog', verbose_name='Image')),
                ('blog', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='blog_image', to='adminpanel.blog')),
            ],
            options={
                'verbose_name_plural': 'Blog Fotoğrafı',
            },
        ),
        migrations.CreateModel(
            name='AmbienteImage',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('ambiente_image', models.ImageField(blank=True, null=True, upload_to='ambiente', verbose_name='Image')),
                ('ambiente', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='ambiente_image', to='adminpanel.ambiente')),
            ],
            options={
                'verbose_name_plural': 'Ambiente Fotoğrafı',
            },
        ),
    ]
