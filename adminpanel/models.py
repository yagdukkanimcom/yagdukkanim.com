from django.db import models
from PIL import Image
from io import BytesIO
from django.template.defaultfilters import slugify
from django.core.files.uploadedfile import InMemoryUploadedFile
import sys
from django.shortcuts import reverse
from unidecode import unidecode
from datetime import datetime
from ckeditor.fields import RichTextField
import random


class Blog(models.Model):
    title = models.CharField(max_length=150, blank=True, null=True)
    context = RichTextField(max_length=3000, verbose_name='İçerik Giriniz', blank="False", null="True")
    slug = models.SlugField(null=True, unique=True, editable=False)
    creationDate = models.DateField(auto_now=True, blank=True, null=True)
    description = models.CharField(max_length=350, blank=True, null=True)

    def __str__(self):
        return str(self.title)

    def get_unique_slug(self):
        sayi=0
        slug = slugify(unidecode(self.title))
        new_slug=slug
        while Blog.objects.filter(slug=new_slug).exists():
            sayi+=1
            new_slug="%s-%s"%(slug,sayi)
        slug = new_slug
        return slug
    
    def get_blog_image(self):
        return self.blog_image.all()
    
    def save(self, *args, **kwargs):
        if self.id is None:
            title = self.get_unique_slug()
            self.slug = slugify(unidecode(title))
        else:
            blog=Blog.objects.get(slug=self.slug)
            if blog.title != self.title:
                self.slug=self.get_unique_slug()
        super(Blog,self).save()

    class Meta:
        verbose_name_plural = "Blog"

class BlogImage(models.Model):
    blog_image = models.ImageField(upload_to='blog',verbose_name='Image', null=True, blank=True)
    blog = models.ForeignKey(Blog, related_name='blog_image',on_delete=models.CASCADE, blank=True, null=True)

    def get_image_url(self):
        return self.blog_image.url

    def save(self, *args, **kwargs):
        imageTemproary = Image.open(self.blog_image)
        imageTemproary1 = imageTemproary.convert('RGB')
        outputIoStream = BytesIO()
        imageTemproaryResized = imageTemproary1.resize( (1800,600) )
        imageTemproaryResized.save(outputIoStream , format='JPEG', quality=85)
        outputIoStream.seek(0)
        self.blog_image = InMemoryUploadedFile(outputIoStream,'ImageField', "%s.jpg" %self.blog_image.name.split('.')[0], 'image/jpeg', sys.getsizeof(outputIoStream), None)
        super(BlogImage, self).save(*args, **kwargs)

    class Meta:
        verbose_name_plural = "Blog Fotoğrafı"


class Slider(models.Model):
    title = models.CharField(max_length=150, blank=True, null=True)
    context = RichTextField(max_length=3000, verbose_name='Slider İçeriği Giriniz', blank="False", null="True")
    slug = models.SlugField(null=True, unique=True, editable=False)
    creationDate = models.DateField(auto_now=True, blank=True, null=True)
    link = models.CharField(max_length=750, blank=True, null=True)

    def __str__(self):
        return str(self.title)

    def get_unique_slug(self):
        sayi=0
        slug = slugify(unidecode(self.title))
        new_slug=slug
        while Slider.objects.filter(slug=new_slug).exists():
            sayi+=1
            new_slug="%s-%s"%(slug,sayi)
        slug = new_slug
        return slug
    
    def get_slider_image(self):
        return self.slider_image.all()
    
    def save(self, *args, **kwargs):
        if self.id is None:
            title = self.get_unique_slug()
            self.slug = slugify(unidecode(title))
        else:
            slider=Slider.objects.get(slug=self.slug)
            if slider.title != self.title:
                self.slug=self.get_unique_slug()
        super(Slider,self).save()

    class Meta:
        verbose_name_plural = "Slider"

class SliderImage(models.Model):
    slider_image = models.ImageField(upload_to='slider',verbose_name='Image', null=True, blank=True)
    slider = models.ForeignKey(Slider, related_name='slider_image',on_delete=models.CASCADE, blank=True, null=True)

    def get_image_url(self):
        return self.slider_image.url

    def save(self, *args, **kwargs):
        imageTemproary = Image.open(self.slider_image)
        imageTemproary1 = imageTemproary.convert('RGB')
        outputIoStream = BytesIO()
        w, h = imageTemproary.size
        imageTemproaryResized = imageTemproary.resize( (int(w/2), int(h/2))  )
        imageTemproaryResized.save(outputIoStream , format='JPEG', quality=85)
        outputIoStream.seek(0)
        self.slider_image = InMemoryUploadedFile(outputIoStream,'ImageField', "%s.jpg" %self.slider_image.name.split('.')[0], 'image/jpeg', sys.getsizeof(outputIoStream), None)
        super(SliderImage, self).save(*args, **kwargs)

    class Meta:
        verbose_name_plural = "Slider Fotoğrafı"




class Mesajlar(models.Model):
    senders_name = models.CharField(max_length=50, blank = True, null=True)
    senders_mail = models.EmailField(max_length=50, null=True)
    tel = models.CharField(max_length=50, blank = True, null=True)
    subject = models.CharField(max_length=50, null=True)
    text = models.CharField(max_length=500, null=True)
    creationDate = models.DateField(auto_now=True, blank=True, null=True)
    
    class Meta:
        verbose_name_plural = "Müşteri Mesajları"

class SSS(models.Model):
    sss_title = models.CharField(max_length=550, blank = True, null=True)
    sss_detail = RichTextField(max_length=5500, verbose_name='SSS İçeriğini Giriniz', blank="False", null="True")
    creationDate = models.DateField(auto_now=True, blank=True, null=True)
    sss_id = models.IntegerField(null=True, blank=True)

    class Meta:
        verbose_name_plural = "SSS"

class Footer(models.Model):
    title = models.CharField(max_length=250, blank=True, null=True)
    slug = models.SlugField(null=True, unique=True, editable=False)
    link = models.CharField(max_length=750, blank=True, null=True)

    def __str__(self):
        return str(self.title)

    def get_unique_slug(self):
        sayi=0
        slug = slugify(unidecode(self.title))
        new_slug=slug
        while Footer.objects.filter(slug=new_slug).exists():
            sayi+=1
            new_slug="%s-%s"%(slug,sayi)
        slug = new_slug
        return slug
    
    def get_footer_image(self):
        return self.footer_image.all()
    
    def save(self, *args, **kwargs):
        if self.id is None:
            title = self.get_unique_slug()
            self.slug = slugify(unidecode(title))
        else:
            footer=Footer.objects.get(slug=self.slug)
            if footer.title != self.title:
                self.slug=self.get_unique_slug()
        super(Footer,self).save()

    class Meta:
        verbose_name_plural = "Footer"

class FooterImage(models.Model):
    footer_image = models.ImageField(upload_to='footer',verbose_name='Image', null=True, blank=True)
    footer = models.ForeignKey(Footer, related_name='footer_image',on_delete=models.CASCADE, blank=True, null=True)

    def get_image_url(self):
        return self.footer_image.url

    def save(self, *args, **kwargs):
        imageTemproary = Image.open(self.footer_image)
        imageTemproary1 = imageTemproary.convert('RGB')
        outputIoStream = BytesIO()
        w, h = imageTemproary.size
        imageTemproaryResized = imageTemproary.resize( (int(w/2), int(h/2))  )
        imageTemproaryResized.save(outputIoStream , format='JPEG', quality=85)
        outputIoStream.seek(0)
        self.footer_image = InMemoryUploadedFile(outputIoStream,'ImageField', "%s.jpg" %self.footer_image.name.split('.')[0], 'image/jpeg', sys.getsizeof(outputIoStream), None)
        super(FooterImage, self).save(*args, **kwargs)

    class Meta:
        verbose_name_plural = "Footer Fotoğrafı"


class Home3(models.Model):
    title = models.CharField(max_length=150, blank=True, null=True)
    context = RichTextField(max_length=3000, verbose_name='İçerik Giriniz', blank="False", null="True")
    slug = models.SlugField(null=True, unique=True, editable=False)
    creationDate = models.DateField(auto_now=True, blank=True, null=True)
    description = models.CharField(max_length=350, blank=True, null=True)

    def __str__(self):
        return str(self.title)

    def get_unique_slug(self):
        sayi=0
        slug = slugify(unidecode(self.title))
        new_slug=slug
        while Home3.objects.filter(slug=new_slug).exists():
            sayi+=1
            new_slug="%s-%s"%(slug,sayi)
        slug = new_slug
        return slug
    
    def get_home_3_image(self):
        return self.home_3_image.all()
    
    def save(self, *args, **kwargs):
        if self.id is None:
            title = self.get_unique_slug()
            self.slug = slugify(unidecode(title))
        else:
            home_3=Home3.objects.get(slug=self.slug)
            if home_3.title != self.title:
                self.slug=self.get_unique_slug()
        super(Home3,self).save()

    class Meta:
        verbose_name_plural = "Home3"

class Home3Image(models.Model):
    home_3_image = models.ImageField(upload_to='home_3',verbose_name='Image', null=True, blank=True)
    home_3 = models.ForeignKey(Home3, related_name='home_3_image',on_delete=models.CASCADE, blank=True, null=True)

    def get_image_url(self):
        return self.home_3_image.url

    def save(self, *args, **kwargs):
        imageTemproary = Image.open(self.home_3_image)
        imageTemproary1 = imageTemproary.convert('RGB')
        outputIoStream = BytesIO()
        imageTemproaryResized = imageTemproary1.resize( (370,570) )
        imageTemproaryResized.save(outputIoStream , format='JPEG', quality=85)
        outputIoStream.seek(0)
        self.home_3_image = InMemoryUploadedFile(outputIoStream,'ImageField', "%s.jpg" %self.home_3_image.name.split('.')[0], 'image/jpeg', sys.getsizeof(outputIoStream), None)
        super(Home3Image, self).save(*args, **kwargs)

    class Meta:
        verbose_name_plural = "Home3 Fotoğrafı"


class Sayfalar(models.Model):
    zollfrei_shopping   = RichTextField(max_length=14000, verbose_name='Zollfrei Shopping', blank="False", null="True")
    zhlungsmethoden     = RichTextField(max_length=14000, verbose_name=' Zahlungsmethoden', blank="False", null="True")
    agbs                = RichTextField(max_length=14000, verbose_name='AGBS', blank="False", null="True")
    impressum           = RichTextField(max_length=14000, verbose_name='Impressum', blank="False", null="True")
    datenschutzerklarung = RichTextField(max_length=14000, verbose_name='Datenschutzerklärung', blank="False", null="True")
    class Meta:
        verbose_name_plural = "Sayfalar"

