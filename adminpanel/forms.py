from ckeditor.widgets import CKEditorWidget
from django import forms
from .models import Slider, Blog, SliderImage, BlogImage , SSS , FooterImage, Footer, Home3, Home3Image, Sayfalar

class BlogForm(forms.ModelForm):
    context = forms.CharField(required=False,widget=CKEditorWidget)
    def __init__(self, *args, **kwargs):
        super(BlogForm,self).__init__(*args, **kwargs)
        for field in self.fields:
            self.fields[field].widget.attrs={'class':'form-control'}
        self.fields['context'].widget.attrs={'rows':10}
    class Meta:
        model = Blog
        fields = '__all__'

class BlogUpdateFormForImage(forms.ModelForm):
    title = forms.CharField(max_length=150, widget=forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'Blog Titel'}))
    class Meta:
        model = Blog
        fields = ('title',)

class BlogImageForm(forms.ModelForm):
    blog_image = forms.ImageField(required=False,label='', widget=forms.ClearableFileInput(attrs={'multiple':'multiple', 'class':'file-input', 'data-fouc':True}))
    class Meta:
        model = BlogImage
        fields = ('blog_image',)


class SliderForm(forms.ModelForm):
    context = forms.CharField(required=False, widget=CKEditorWidget)
    link = forms.CharField(required=False,max_length=150, widget=forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'Slider Verknüpfung'}))

    def __init__(self, *args, **kwargs):
        super(SliderForm,self).__init__(*args, **kwargs)
        for field in self.fields:
            self.fields[field].widget.attrs={'class':'form-control'}
        self.fields['context'].widget.attrs={'rows':10}
    class Meta:
        model = Slider
        fields = '__all__'

class SliderUpdateFormForImage(forms.ModelForm):
    title = forms.CharField(required=False, max_length=150, widget=forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'Slider-Header'}))
    link = forms.CharField(required=False,max_length=150, widget=forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'Slider Verknüpfung'}))

    class Meta:
        model = Slider
        fields = ('title','link')

class SliderImageForm(forms.ModelForm):
    slider_image = forms.ImageField(required=False, label='', widget=forms.ClearableFileInput(attrs={'multiple':'multiple', 'class':'file-input', 'data-fouc':True ,'placeholder': 'Schieberegler Foto'}))
    class Meta:
        model = SliderImage
        fields = ('slider_image',)

class SSSForm(forms.ModelForm):
    sss_title = forms.CharField(max_length=100, widget=forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'Frage'}))
    sss_detail = forms.CharField(widget=CKEditorWidget)
    sss_id = forms.IntegerField(widget=forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'ID für die Sequenzierung'}))

    def __init__(self, *args, **kwargs):
        super(SSSForm,self).__init__(*args, **kwargs)
        self.fields['sss_detail'].widget.attrs={'rows':10}

    class Meta:
        model = SSS
        fields = ['sss_id','sss_title','sss_detail']

"""
class AddCommentsForm(forms.ModelForm):
    senders_name = forms.CharField(max_length=150, widget=forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'Ad Soyad'}))
    senders_mail = forms.CharField(max_length=150, widget=forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'E Posta'}))
    text = forms.CharField(max_length=750, widget=forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'Yorum'}))
    
    class Meta:
        model = InstructorComments
        fields = ('senders_name','senders_mail','text')
"""



class FooterForm(forms.ModelForm):
    title = forms.CharField(required=False, max_length=250, widget=forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'Footer-Header'}))
    link = forms.CharField(required=False,max_length=750, widget=forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'Footer Verknüpfung'}))


    class Meta:
        model = Footer
        fields = '__all__'

class FooterUpdateFormForImage(forms.ModelForm):
    title = forms.CharField(required=False, max_length=250, widget=forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'Footer-Header'}))
    link = forms.CharField(required=False,max_length=750, widget=forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'Footer Verknüpfung'}))

    class Meta:
        model = Footer
        fields = ('title','link')

class FooterImageForm(forms.ModelForm):
    footer_image = forms.ImageField(required=False, label='', widget=forms.ClearableFileInput(attrs={'multiple':'multiple', 'class':'file-input', 'data-fouc':True ,'placeholder': 'Footer Foto'}))
    class Meta:
        model = FooterImage
        fields = ('footer_image',)


class Home3Form(forms.ModelForm):
    context = forms.CharField(required=False,widget=CKEditorWidget)
    def __init__(self, *args, **kwargs):
        super(Home3Form,self).__init__(*args, **kwargs)
        for field in self.fields:
            self.fields[field].widget.attrs={'class':'form-control'}
        self.fields['context'].widget.attrs={'rows':10}
    class Meta:
        model = Home3
        fields = '__all__'

class Home3UpdateFormForImage(forms.ModelForm):
    title = forms.CharField(required=False, max_length=150, widget=forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'Home3 Titel'}))
    class Meta:
        model = Home3
        fields = ('title',)

class Home3ImageForm(forms.ModelForm):
    home_3_image = forms.ImageField(required=False,label='', widget=forms.ClearableFileInput(attrs={'multiple':'multiple', 'class':'file-input', 'data-fouc':True}))
    class Meta:
        model = Home3Image
        fields = ('home_3_image',)



class SayfalarForm(forms.ModelForm):
    zollfrei_shopping   = forms.CharField(required=False,widget=CKEditorWidget)
    zhlungsmethoden     = forms.CharField(required=False,widget=CKEditorWidget)
    agbs                = forms.CharField(required=False,widget=CKEditorWidget)
    impressum           = forms.CharField(required=False,widget=CKEditorWidget)
    datenschutzerklarung = forms.CharField(required=False,widget=CKEditorWidget)
    class Meta:
        model = Sayfalar
        fields = '__all__'