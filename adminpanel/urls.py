from django.urls import path
from . import views
from .view import blog, company, mesaj, slider , sss ,  footerimage, home_3, pages

app_name = 'adminpanel'

urlpatterns = [
    path('', views.index, name="index"),

    path('update-company-image/', company.update_company_image, name="update-company-image"),
    path('update-company-info/', company.update_company_info, name="update-company-info"),

    path('delete-mesaj/<int:pk>/', mesaj.delete_mesaj, name="delete-mesaj"),
    path('list-mesaj/', mesaj.list_mesaj, name="list-mesaj"),
    path('message-detail/<int:pk>/', mesaj.detail_mesaj, name="detail-mesaj"),
    path('add-mesaj/', mesaj.add_mesaj, name="add-mesaj"),
    path('update-sayfalar/', pages.update_sayfalar, name="update-sayfalar"),


    path('delete-sss/<int:pk>/', sss.delete_sss, name="delete-sss"),
    path('list-sss/', sss.list_sss, name="list-sss"),
    path('sss-detail/<int:pk>/', sss.detail_sss, name="detail-sss"),
    path('add-sss/', sss.add_sss, name="add-sss"),



    path('add-slider-images/', slider.add_slider_images, name="add-slider-images"),

    path('add-slider/', slider.add_slider, name="add-slider"),
    path('update-slider/<int:pk>/', slider.update_slider, name="update-slider"),
    path('detail-slider/<int:pk>/', slider.detail_slider, name="detail-slider"),
    path('delete-slider/<int:pk>/', slider.delete_slider, name="delete-slider"),
    path('list-slider/', slider.list_slider, name="list-slider"),
    path('update-slider-image/<int:pk>/', slider.update_slider_image, name="update-slider-image"),



    path('add-footer-images/', footerimage.add_footer_images, name="add-footer-images"),

    path('add-footer/', footerimage.add_footer, name="add-footer"),
    path('update-footer/<int:pk>/', footerimage.update_footer, name="update-footer"),
    path('detail-footer/<int:pk>/', footerimage.detail_footer, name="detail-footer"),
    path('delete-footer/<int:pk>/', footerimage.delete_footer, name="delete-footer"),
    path('list-footer/', footerimage.list_footer, name="list-footer"),
    path('update-footer-image/<int:pk>/', footerimage.update_footer_image, name="update-footer-image"),


    path('add-home-3/', home_3.add_home_3, name="add-home-3"),
    path('update-home-3/<int:pk>/', home_3.update_home_3, name="update-home-3"),
    path('detail-home-3/<int:pk>/', home_3.detail_home_3, name="detail-home-3"),
    path('delete-home-3/<int:pk>/', home_3.delete_home_3, name="delete-home-3"),
    path('list-home-3/', home_3.list_home_3, name="list-home-3"),
    path('update-home-3-image/<int:pk>/', home_3.update_home_3_image, name="update-home-3-image"),



    path('add-blog/', blog.add_blog, name="add-blog"),
    path('update-blog/<int:pk>/', blog.update_blog, name="update-blog"),
    path('detail-blog/<int:pk>/', blog.detail_blog, name="detail-blog"),
    path('delete-blog/<int:pk>/', blog.delete_blog, name="delete-blog"),
    path('list-blog/', blog.list_blog, name="list-blog"),
    path('update-blog-image/<int:pk>/', blog.update_blog_image, name="update-blog-image"),

]
