from django.shortcuts import render, redirect, get_object_or_404, reverse
from django.contrib import messages
from ..models import Sayfalar
from ..forms import SayfalarForm

def update_sayfalar(request):
    sayfalar = Sayfalar.objects.all()[0]
    if request.POST:
        form = SayfalarForm(instance=sayfalar, data=request.POST or None)
        if form.is_valid():
            form.save()
            messages.success(request, 'Seiten erfolgreich aktualisieren')
            return redirect('adminpanel:update-sayfalar')
        else:
            messages.error(request, 'Aktualisieren der Seiten fehlgeschlagen')
    else:
        form = SayfalarForm(instance=sayfalar)

    return render(request, 'adminpanel/sayfalar/update_sayfalar.html', {'form':form, 'pageheader': 'Sayfaları Güncelle'})
