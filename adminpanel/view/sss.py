from django.shortcuts import render, redirect, get_object_or_404, reverse
from django.contrib import messages
from django.core.exceptions import ObjectDoesNotExist

from ..models import SSS
from ..forms import SSSForm
from accounts.views import get_auth

def add_sss(request):
    flag, gidilecek_sayfa = get_auth(request)
    if flag:
        return redirect(gidilecek_sayfa)
        
    form = SSSForm()
    if request.POST:
        form = SSSForm(request.POST)
        if form.is_valid() :
            sss = form.save()

            messages.success(request, 'İletiniz Başarıyla Gönderildi.')
            return redirect('adminpanel:list-sss')

    context = {'form':form,'pageheader':'SSS Ekle'}
    return render(request, 'adminpanel/sss/add_sss.html', context)

def delete_sss(request, pk):
    flag, gidilecek_sayfa = get_auth(request)
    if flag:
        return redirect(gidilecek_sayfa)

    sss = get_object_or_404(SSS, pk=pk)
    sss.delete()
    messages.success(request, 'Silme İşlemi Başarılı')
    return redirect('adminpanel:list-sss')

def detail_sss(request, pk):
    flag, gidilecek_sayfa = get_auth(request)
    if flag:
        return redirect(gidilecek_sayfa)

    sss = get_object_or_404(SSS, pk=pk)

    form = SSSForm(instance=sss, data=request.POST or None)

    if request.POST:
        if  form.is_valid() :
            sss = form.save()

            messages.success(request, 'İletiniz Başarıyla Güncellendi.')
            return redirect('adminpanel:list-sss')
    context = {'form':form,'sss':sss,'pageheader':'SSS Detay'}
    return render(request, 'adminpanel/sss/sss_detail.html', context)

def list_sss(request):
    flag, gidilecek_sayfa = get_auth(request)
    if flag:
        return redirect(gidilecek_sayfa)
    sss = SSS.objects.all()
    context = {'sss':sss,'pageheader':'SSS'}
    return render(request, 'adminpanel/sss/list_sss.html', context)
