from django.shortcuts import render, redirect, get_object_or_404, reverse
from django.contrib import messages
from django.core.exceptions import ObjectDoesNotExist
from accounts.models import CompanyInfo, CompanyImage
from accounts.forms import CompanyInfoForm, CompanyImageForm
from accounts.views import get_auth

def update_company_image(request):
    flag, gidilecek_sayfa = get_auth(request)
    if flag:
        return redirect(gidilecek_sayfa)
        
    company_images = CompanyImage.objects.all()
    company_image = company_images[0]
    if request.POST:
        form = CompanyImageForm(instance=company_image,data=request.POST,files=request.FILES)
        if form.is_valid():
            form.save()
            messages.success(request, 'Firma Fotoğrafı Güncelleme İşlemi Başarılı')
            return redirect('adminpanel:update-company-image')
        else:
            messages.error(request, 'Firma Fotoğrafı Güncelleme İşlemi Başarısız')
    else:
        form = CompanyImageForm(instance=company_image, data=request.POST, files=request.FILES)

    return render(request, 'adminpanel/company/update_company_image.html', {'form':form, 'pageheader': 'Firma Fotoğrafını Güncelle'})

def update_company_info(request):
    flag, gidilecek_sayfa = get_auth(request)
    if flag:
        return redirect(gidilecek_sayfa)
    
    companys = CompanyInfo.objects.all()
    company = companys[0]
    if request.POST:
        form = CompanyInfoForm(instance=company, data=request.POST or None)
        if form.is_valid():
            form.save()
            messages.success(request, 'Firma Bilgileri Güncelleme İşlemi Başarılı')
            return redirect('adminpanel:index')
        else:
            messages.error(request, 'Firma Bilgileri Güncelleme İşlemi Başarısız')
    else:
        form = CompanyInfoForm(instance=company)

    return render(request, 'adminpanel/company/update_company_info.html', {'form':form, 'pageheader': 'Firma Bilgilerini Güncelle'})
