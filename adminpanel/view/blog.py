from django.shortcuts import render, get_object_or_404, redirect
from django.contrib import messages
from ..models import Blog , BlogImage
from ..forms import BlogForm , BlogImageForm , BlogUpdateFormForImage
from googletrans import Translator


def delete_blog(request,pk):
    blog = get_object_or_404(Blog, pk=pk)
    blog.delete()
    messages.success(request, 'Silme İşlemi Başarılı')
    return redirect('adminpanel:list-blog')

def detail_blog(request, pk):
    blog = get_object_or_404(Blog, pk=pk)
    context = {'blog': blog, 'pageheader': 'Bloglar'}
    return render(request, 'adminpanel/blog/detail_blog.html', context)

def list_blog(request):
    blogs = Blog.objects.all()
    context = {'blogs': blogs, 'pageheader': 'Bloglar'}
    return render(request, 'adminpanel/blog/list_blog.html', context)

def update_blog(request,pk):
    blog = get_object_or_404(Blog, pk=pk)
    form = BlogForm(instance=blog, data=request.POST or None)
    if request.POST:
        if form.is_valid():
            form.save()
            messages.success(request, 'Güncelleme İşlemi Başarılı')
            return redirect('adminpanel:list-blog')
        else:
            messages.error(request, 'Güncelleme İşlemi Başarısız')

    context = {'form': form, 'pageheader': 'Blog Güncelle'}
    return render(request, 'adminpanel/blog/update_blog.html', context)

def add_blog(request):

    if request.POST:
        form = BlogForm(request.POST)
        imageForm = BlogImageForm(request.POST, request.FILES)
        files = request.FILES.getlist('blog_image')

        if form.is_valid() and imageForm.is_valid() and files:
            blog = form.save()
            numberOfPhotosToUpload = 8
            countOfPhotosUploaded = 0
            for f in files:
                countOfPhotosUploaded +=1
                uploadedPhoto = BlogImage(blog=blog,blog_image=f)
                if countOfPhotosUploaded <= numberOfPhotosToUpload:
                    uploadedPhoto.save()

            messages.success(request, 'Blog Ekleme İşlemi Başarılı')
            return redirect('adminpanel:list-blog')
        
        else:
            messages.error(request, 'Blog Ekleme İşlemi Başarısız')
    else:
        form = BlogForm()
        imageForm = BlogImageForm(request.POST, request.FILES)

    context = {'form': form, 'pageheader': 'Blog Güncelle', "imageForm":imageForm, }
    return render(request, 'adminpanel/blog/add_blog.html', context)


def update_blog_image(request, pk):

    blog = get_object_or_404(Blog, pk=pk)
    form = BlogUpdateFormForImage(instance=blog, data=request.POST or None)

    imageQuery = []
    for img in blog.get_blog_image():
        imageQuery.append(BlogImage.objects.get(pk=img.pk))
        BlogImage.objects.get(pk=img.pk).delete()

    image_sayisi = 8 - len(imageQuery)

    imageForm = BlogImageForm(data=request.POST or None, files=request.FILES or None)

    if request.POST:

        imageForm = BlogImageForm(data=request.POST,files=request.FILES)
        files = request.FILES.getlist('blog_image')
        if  form.is_valid() and imageForm.is_valid():
            blog = form.save()

            countOfPhotosUploaded = 0
            for f in files:
                countOfPhotosUploaded +=1
                uploadedPhoto = BlogImage(blog=blog,blog_image=f)
                if countOfPhotosUploaded <= image_sayisi:
                    uploadedPhoto.save()

            return redirect('adminpanel:list-blog')
        else:
            messages.error(request, 'Blog Fotoğrafı Güncelleme İşlemi Başarısız1')

        messages.success(request, 'Blog Fotoğrafı Güncelleme İşlemi Başarılı')
        return redirect('adminpanel:list-blog')


    return render(request, 'adminpanel/blog/update_blog_image.html', context={'pageheader': 'Blog Fotoğraf Güncelle','form':form, 'imageForm':imageForm })
