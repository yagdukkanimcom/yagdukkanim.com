from django.shortcuts import render, redirect, get_object_or_404, reverse
from django.contrib import messages
from django.core.exceptions import ObjectDoesNotExist

from ..models import Mesajlar
from accounts.views import get_auth

def add_mesaj(request):
    flag, gidilecek_sayfa = get_auth(request)
    if flag:
        return redirect(gidilecek_sayfa)

    if request.POST:
        ad_soyad = request.POST.get('ad-soyad')
        email = request.POST.get('email')
        subject = request.POST.get('subject')
        text = request.POST.get('text')

        mesaj = Mesajlar.objects.create(senders_name=ad_soyad, senders_mail=email, subject=subject, text=text)
        mesaj.save()
        messages.success(request, 'Ihre Nachricht wurde erfolgreich gesendet.')
        return redirect('website:contact')

    context = {'pageheader':'Mesaj Ekle'}
    return render(request, 'adminpanel/mesajlar/add_mesaj.html', context)

def delete_mesaj(request, pk):
    flag, gidilecek_sayfa = get_auth(request)
    if flag:
        return redirect(gidilecek_sayfa)

    mesaj = get_object_or_404(Mesajlar, pk=pk)
    mesaj.delete()
    messages.success(request, 'Löschung erfolgreich')
    return redirect('adminpanel:list-mesaj')

def detail_mesaj(request, pk):
    flag, gidilecek_sayfa = get_auth(request)
    if flag:
        return redirect(gidilecek_sayfa)

    mesaj = get_object_or_404(Mesajlar, pk=pk)
    context = {'mesaj':mesaj,'pageheader':'Botschaft Detail'}
    return render(request, 'adminpanel/mesajlar/mesaj_detail.html', context)

def list_mesaj(request):
    flag, gidilecek_sayfa = get_auth(request)
    if flag:
        return redirect(gidilecek_sayfa)
    mesajlar = Mesajlar.objects.all()
    context = {'mesajlar':mesajlar,'pageheader':'Mitteilungen'}
    return render(request, 'adminpanel/mesajlar/mesajlar.html', context)
