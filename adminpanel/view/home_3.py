from django.shortcuts import render, get_object_or_404, redirect
from django.contrib import messages
from ..models import Home3 , Home3Image
from ..forms import Home3Form , Home3ImageForm , Home3UpdateFormForImage
from googletrans import Translator


def delete_home_3(request,pk):
    home_3 = get_object_or_404(Home3, pk=pk)
    home_3.delete()
    messages.success(request, 'Silme İşlemi Başarılı')
    return redirect('adminpanel:list-home-3')

def detail_home_3(request, pk):
    home_3 = get_object_or_404(Home3, pk=pk)
    context = {'home_3': home_3, 'pageheader': 'Home3lar'}
    return render(request, 'adminpanel/home3/detail_home3.html', context)

def list_home_3(request):
    home_3 = Home3.objects.all()
    context = {'home_3': home_3, 'pageheader': 'Home3lar'}
    return render(request, 'adminpanel/home3/list_home3.html', context)

def update_home_3(request,pk):
    home_3 = get_object_or_404(Home3, pk=pk)
    form = Home3Form(instance=home_3, data=request.POST or None)
    if request.POST:
        if form.is_valid():
            form.save()
            messages.success(request, 'Güncelleme İşlemi Başarılı')
            return redirect('adminpanel:list-home-3')
        else:
            messages.error(request, 'Güncelleme İşlemi Başarısız')

    context = {'form': form, 'pageheader': 'Home3 Güncelle'}
    return render(request, 'adminpanel/home3/update_home3.html', context)

def add_home_3(request):

    if request.POST:
        form = Home3Form(request.POST)
        imageForm = Home3ImageForm(request.POST, request.FILES)
        files = request.FILES.getlist('home_3_image')

        if form.is_valid() and imageForm.is_valid() and files:
            home_3 = form.save()
            numberOfPhotosToUpload = 8
            countOfPhotosUploaded = 0
            for f in files:
                countOfPhotosUploaded +=1
                uploadedPhoto = Home3Image(home_3=home_3,home_3_image=f)
                if countOfPhotosUploaded <= numberOfPhotosToUpload:
                    uploadedPhoto.save()

            messages.success(request, 'Home3 Ekleme İşlemi Başarılı')
            return redirect('adminpanel:list-home-3')
        
        else:
            messages.error(request, 'Home3 Ekleme İşlemi Başarısız')
    else:
        form = Home3Form()
        imageForm = Home3ImageForm(request.POST, request.FILES)

    context = {'form': form, 'pageheader': 'Home3 Güncelle', "imageForm":imageForm, }
    return render(request, 'adminpanel/home3/add_home3.html', context)


def update_home_3_image(request, pk):

    home_3 = get_object_or_404(Home3, pk=pk)
    form = Home3UpdateFormForImage(instance=home_3, data=request.POST or None)

    imageQuery = []
    for img in home_3.get_home_3_image():
        imageQuery.append(Home3Image.objects.get(pk=img.pk))
        Home3Image.objects.get(pk=img.pk).delete()

    image_sayisi = 8 - len(imageQuery)

    imageForm = Home3ImageForm(data=request.POST or None, files=request.FILES or None)

    if request.POST:

        imageForm = Home3ImageForm(data=request.POST,files=request.FILES)
        files = request.FILES.getlist('home_3_image')
        if  form.is_valid() and imageForm.is_valid():
            home_3 = form.save()

            countOfPhotosUploaded = 0
            for f in files:
                countOfPhotosUploaded +=1
                uploadedPhoto = Home3Image(home_3=home_3,home_3_image=f)
                if countOfPhotosUploaded <= image_sayisi:
                    uploadedPhoto.save()

            return redirect('adminpanel:list-home-3')
        else:
            messages.error(request, 'Home3 Fotoğrafı Güncelleme İşlemi Başarısız1')

        messages.success(request, 'Home3 Fotoğrafı Güncelleme İşlemi Başarılı')
        return redirect('adminpanel:list-home-3')


    return render(request, 'adminpanel/home3/update_home3_image.html', context={'pageheader': 'Home3 Fotoğraf Güncelle','form':form, 'imageForm':imageForm })
