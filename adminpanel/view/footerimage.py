from django.shortcuts import render, redirect, get_object_or_404, reverse
from django.contrib import messages
from django.core.exceptions import ObjectDoesNotExist

from ..forms import FooterImageForm , FooterImage , FooterForm , FooterUpdateFormForImage
from ..models import FooterImage , Footer 
from accounts.views import get_auth

def add_footer_images(request):
    flag, gidilecek_sayfa = get_auth(request)
    if flag:
        return redirect(gidilecek_sayfa)

    if request.POST:
        FooterImage.objects.all().delete()
        imageForm = FooterImageForm(request.POST, request.FILES)
        files = request.FILES.getlist('footer_image')

        if imageForm.is_valid():
            count = 0
            for f in files:
                count +=1
                k = FooterImage(footer_image=f)
                if count <=3:
                    k.save()

            return redirect('adminpanel:add-footer-images')
    else:
        imageForm = FooterImageForm(request.POST, request.FILES)

    context={'imageForm':imageForm, 'pageheader': 'Footer-Foto aktualisieren '}
    return render(request, 'adminpanel/footerimage/add_footerimage_images.html', context)


def delete_footer(request,pk):
    footer = get_object_or_404(Footer, pk=pk)
    footer.delete()
    messages.success(request, 'Löschung erfolgreich')
    return redirect('adminpanel:list-footer')

def detail_footer(request, pk):
    footer = get_object_or_404(Footer, pk=pk)
    context = {'footer': footer, 'pageheader': 'Schieberegler Detail'}
    return render(request, 'adminpanel/footerimage/detail_footer.html', context)

def list_footer(request):
    footers = Footer.objects.all()
    context = {'footers': footers, 'pageheader': 'Schieberegler'}
    return render(request, 'adminpanel/footerimage/list_footerimage.html', context)

def update_footer(request,pk):
    footer = get_object_or_404(Footer, pk=pk)
    form = FooterForm(instance=footer, data=request.POST or None)
    if request.POST:
        if form.is_valid():
            form.save()
            messages.success(request, 'Aktualisierung erfolgreich')
            return redirect('adminpanel:list-footer')
        else:
            messages.error(request, 'Aktualisierung erfolglos')

    context = {'form': form, 'pageheader': 'Footer Aktualisieren'}
    return render(request, 'adminpanel/footerimage/update_footerimage.html', context)

def add_footer(request):

    if request.POST:
        form = FooterForm(request.POST)
        imageForm = FooterImageForm(request.POST, request.FILES)
        files = request.FILES.getlist('footer_image')

        if form.is_valid() and imageForm.is_valid() and files:
            footer = form.save()
            numberOfPhotosToUpload = 8
            countOfPhotosUploaded = 0
            for f in files:
                countOfPhotosUploaded +=1
                uploadedPhoto = FooterImage(footer=footer,footer_image=f)
                if countOfPhotosUploaded <= numberOfPhotosToUpload:
                    uploadedPhoto.save()

            messages.success(request, 'Footer erfolgreich hinzufügen')
            return redirect('adminpanel:list-footer')
        
        else:
            messages.error(request, 'Hinzufügen des Footer fehlgeschlagen')
    else:
        form = FooterForm()
        imageForm = FooterImageForm(request.POST, request.FILES)

    context = {'form': form, 'pageheader': 'Footer Hinzufügen', "imageForm":imageForm, }
    return render(request, 'adminpanel/footerimage/add_footerimage.html', context)


def update_footer_image(request, pk):

    footer = get_object_or_404(Footer, pk=pk)
    form = FooterUpdateFormForImage(instance=footer, data=request.POST or None)

    imageQuery = []
    for img in footer.get_footer_image():
        imageQuery.append(FooterImage.objects.get(pk=img.pk))
        FooterImage.objects.get(pk=img.pk).delete()

    image_sayisi = 8 - len(imageQuery)

    imageForm = FooterImageForm(data=request.POST or None, files=request.FILES or None)

    if request.POST:

        imageForm = FooterImageForm(data=request.POST,files=request.FILES)
        files = request.FILES.getlist('footer_image')
        if  form.is_valid() and imageForm.is_valid():
            footer = form.save()

            countOfPhotosUploaded = 0
            for f in files:
                countOfPhotosUploaded +=1
                uploadedPhoto = FooterImage(footer=footer,footer_image=f)
                if countOfPhotosUploaded <= image_sayisi:
                    uploadedPhoto.save()

            return redirect('adminpanel:list-footer')
        else:
            messages.error(request, 'Footer Photo Update Fehlgeschlagen')

        messages.success(request, 'Footer Photo Update Erfolgreich')
        return redirect('adminpanel:list-footer')


    return render(request, 'adminpanel/footerimage/update_footerimage_image.html', context={'pageheader': 'Footer-Foto aktualisieren','form':form, 'imageForm':imageForm })
