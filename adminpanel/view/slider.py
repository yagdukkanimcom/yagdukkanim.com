from django.shortcuts import render, redirect, get_object_or_404, reverse
from django.contrib import messages
from django.core.exceptions import ObjectDoesNotExist

from ..forms import SliderImageForm , SliderImage , SliderForm , SliderUpdateFormForImage
from ..models import SliderImage , Slider 
from accounts.views import get_auth

def add_slider_images(request):
    flag, gidilecek_sayfa = get_auth(request)
    if flag:
        return redirect(gidilecek_sayfa)

    if request.POST:
        SliderImage.objects.all().delete()
        imageForm = SliderImageForm(request.POST, request.FILES)
        files = request.FILES.getlist('slider_image')

        if imageForm.is_valid():
            count = 0
            for f in files:
                count +=1
                k = SliderImage(slider_image=f)
                if count <=3:
                    k.save()

            return redirect('adminpanel:add-slider-images')
    else:
        imageForm = SliderImageForm(request.POST, request.FILES)

    context={'imageForm':imageForm, 'pageheader': 'Slider-Foto aktualisieren '}
    return render(request, 'adminpanel/slider/add_slider_images.html', context)


def delete_slider(request,pk):
    slider = get_object_or_404(Slider, pk=pk)
    slider.delete()
    messages.success(request, 'Löschung erfolgreich')
    return redirect('adminpanel:list-slider')

def detail_slider(request, pk):
    slider = get_object_or_404(Slider, pk=pk)
    context = {'slider': slider, 'pageheader': 'Schieberegler Detail'}
    return render(request, 'adminpanel/slider/detail_slider.html', context)

def list_slider(request):
    sliders = Slider.objects.all()
    context = {'sliders': sliders, 'pageheader': 'Schieberegler'}
    return render(request, 'adminpanel/slider/list_slider.html', context)

def update_slider(request,pk):
    slider = get_object_or_404(Slider, pk=pk)
    form = SliderForm(instance=slider, data=request.POST or None)
    if request.POST:
        if form.is_valid():
            form.save()
            messages.success(request, 'Aktualisierung erfolgreich')
            return redirect('adminpanel:list-slider')
        else:
            messages.error(request, 'Aktualisierung erfolglos')

    context = {'form': form, 'pageheader': 'Slider Aktualisieren'}
    return render(request, 'adminpanel/slider/update_slider.html', context)

def add_slider(request):

    if request.POST:
        form = SliderForm(request.POST)
        imageForm = SliderImageForm(request.POST, request.FILES)
        files = request.FILES.getlist('slider_image')

        if form.is_valid() and imageForm.is_valid() and files:
            slider = form.save()
            numberOfPhotosToUpload = 8
            countOfPhotosUploaded = 0
            for f in files:
                countOfPhotosUploaded +=1
                uploadedPhoto = SliderImage(slider=slider,slider_image=f)
                if countOfPhotosUploaded <= numberOfPhotosToUpload:
                    uploadedPhoto.save()

            messages.success(request, 'Slider erfolgreich hinzufügen')
            return redirect('adminpanel:list-slider')
        
        else:
            messages.error(request, 'Hinzufügen des Slider fehlgeschlagen')
    else:
        form = SliderForm()
        imageForm = SliderImageForm(request.POST, request.FILES)

    context = {'form': form, 'pageheader': 'Slider Hinzufügen', "imageForm":imageForm, }
    return render(request, 'adminpanel/slider/add_slider.html', context)


def update_slider_image(request, pk):

    slider = get_object_or_404(Slider, pk=pk)
    form = SliderUpdateFormForImage(instance=slider, data=request.POST or None)

    imageQuery = []
    for img in slider.get_slider_image():
        imageQuery.append(SliderImage.objects.get(pk=img.pk))
        SliderImage.objects.get(pk=img.pk).delete()

    image_sayisi = 8 - len(imageQuery)

    imageForm = SliderImageForm(data=request.POST or None, files=request.FILES or None)

    if request.POST:

        imageForm = SliderImageForm(data=request.POST,files=request.FILES)
        files = request.FILES.getlist('slider_image')
        if  form.is_valid() and imageForm.is_valid():
            slider = form.save()

            countOfPhotosUploaded = 0
            for f in files:
                countOfPhotosUploaded +=1
                uploadedPhoto = SliderImage(slider=slider,slider_image=f)
                if countOfPhotosUploaded <= image_sayisi:
                    uploadedPhoto.save()

            return redirect('adminpanel:list-slider')
        else:
            messages.error(request, 'Slider Photo Update Fehlgeschlagen')

        messages.success(request, 'Slider Photo Update Erfolgreich')
        return redirect('adminpanel:list-slider')


    return render(request, 'adminpanel/slider/update_slider_image.html', context={'pageheader': 'Slider-Foto aktualisieren','form':form, 'imageForm':imageForm })
