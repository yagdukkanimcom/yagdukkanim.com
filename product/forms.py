from django import forms
from .models import Product, ProductImage, ProductStock,ProductsComments, BrandImage, Brand
from ckeditor.widgets import CKEditorWidget
from category.models import Size, Color

PREPARATİON_CHOICES = [
    ('1', '1'),
    ('2', '2'),
    ('3', '3'),
    ('4', '4'),
    ('5', '5'),
    ('6', '6'),
    ('7 ve üzeri', '7 ve Üzeri'),
]

class ProductImageForm(forms.ModelForm):
    product_image = forms.ImageField(required=False,label='', widget=forms.ClearableFileInput(attrs={'multiple':'multiple', 'class':'file-input', 'data-fouc':True}))
    class Meta:
        model = ProductImage
        fields = ('product_image',)


class AddProductUpdateForm(forms.ModelForm):
    product_name = forms.CharField(max_length=80, widget=forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'Produktname'}))
    class Meta:
        model = Product
        fields = ['product_name']



    # product_color = forms.ModelChoiceField(queryset=Color.objects.all(),widget=forms.Select(attrs={'class':'selectpicker', 'data-live-search':'true'}))
    # product_size = forms.MultipleChoiceField(required=False,choices=SIZE_TYPE, widget=forms.CheckboxSelectMultiple())
    # product_materials = forms.CharField(required=False,max_length=80, widget=forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'Produkte Materials'}))
	# product_price_sale = forms.FloatField(required=False, widget=forms.TextInput(attrs={'type':'number', 'class': 'form-control', 'placeholder': 'İndirimli Fiyat'}))
    # product_price2 = forms.FloatField(required=False,widget=forms.TextInput(attrs={'type':'number', 'class': 'form-control', 'placeholder': 'Produktpreis EUR.'}))
    # product_price2_sale = forms.FloatField(required=False,widget=forms.TextInput(attrs={'type':'number', 'class': 'form-control', 'placeholder': 'Produktpreis EUR. SALE'}))


class AddProductForm(forms.ModelForm):
    product_name = forms.CharField(label = 'Başlık', max_length=100, widget=forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'Başlık'}))
    product_detail = forms.CharField(label = 'Detay', widget=CKEditorWidget(attrs={'class': 'form-control'}))
    product_brand = forms.ModelChoiceField(label = 'Marka',queryset=Brand.objects.all(),widget=forms.Select(attrs={'class':'selectpicker form-control', 'data-live-search':'true'}))
    product_price = forms.FloatField(label = 'Fiyatı',required=False, min_value=0, widget=forms.NumberInput(attrs={ 'step': "0.1", 'type':'number', 'class': 'form-control ', 'placeholder': 'Fiyat'}))
    product_discount_rate = forms.FloatField(label = 'İndirim Oranı', initial=0 ,required=False, widget=forms.NumberInput(attrs={'step': "0.1", 'type':'number', 'class': 'form-control ', 'placeholder': 'İndirim Oranı'}))
    product_KDV_rate = forms.FloatField(label = 'KDV Oranı', initial=0 ,required=False, widget=forms.NumberInput(attrs={'step': "0.1", 'type':'number', 'class': 'form-control ', 'placeholder': 'KDV Oranı'}))
    product_code = forms.CharField(label = 'Ürün Kodu',max_length=100, widget=forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'Ürün Kodu'}))
    product_total_quantity = forms.IntegerField(label = 'Stok',required=False, widget=forms.TextInput(attrs={'type':'number', 'class': 'form-control ', 'placeholder': 'Stok'}))
    product_special = forms.BooleanField(label = 'Özel', required=False)
    product_popular = forms.BooleanField(label = 'Popüler', required=False)
    product_sale    = forms.BooleanField(label = 'İndirimli', required=False)
    product_tags = forms.CharField(label = 'Etiketler', max_length=150, widget=forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'Etiketleri aralarına virgül koyarak giriniz.'}))

    product_preparation_time = forms.ChoiceField(label = 'Ürün Hazırlık Süresi',
        widget=forms.RadioSelect,	
        choices = PREPARATİON_CHOICES,
        required=False,
    )

    def __init__(self, *args, **kwargs):
	    super(AddProductForm,self).__init__(*args, **kwargs)
	    self.fields['product_detail'].widget.attrs={'rows':10}

    class Meta:
	    model = Product
	    fields = ['product_name','product_detail','product_brand','product_price','product_discount_rate','product_KDV_rate',
                'product_total_quantity', 'product_code', 'product_tags',
				'product_special','product_popular','product_sale', 'product_preparation_time', ]

class AddProductStockForm(forms.ModelForm):
    product_quantity = forms.CharField(max_length=80, widget=forms.TextInput(attrs={'rows':5,'class': 'form-control', 'placeholder': 'Enter Product Quantity'}))
    product      = forms.ModelChoiceField(queryset=Product.objects.all(),widget=forms.Select(attrs={'class':'selectpicker', 'data-live-search':'true'}))
    product_size = forms.ModelChoiceField(queryset=Size.objects.all(),widget=forms.Select(attrs={'class':'selectpicker', 'data-live-search':'true'}))
    class Meta:
        model = ProductStock
        fields = ['product_size','product','product_quantity']

class AddCommentsForm(forms.ModelForm):
    senders_name = forms.CharField(max_length=150, widget=forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'Name'}))
    senders_mail = forms.CharField(max_length=150, widget=forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'E Posta'}))
    text = forms.CharField(max_length=750, widget=forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'Kommentar'}))
    
    class Meta:
        model = ProductsComments
        fields = ('senders_name','senders_mail','text')

class BrandForm(forms.ModelForm):
    brand_name = forms.CharField(label="Marka", max_length=50, widget=forms.TextInput(attrs={'rows':5,'class': 'form-control', 'placeholder': 'Marka'})) 
    description = forms.CharField(label="Marka Açıklama", required=False, max_length=350, widget=forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'Marka Açıklama'}))
    top_brand = forms.BooleanField(label = "Top", required=False)
	
	# top_brand = forms.ChoiceField(
    #     widget=forms.RadioSelect,
    #     choices = TOP_CHOICES,
    #     required=False,
    # )


    class Meta:
        model = Brand
        fields = ('brand_name','description','top_brand')

class BrandUpdateFormForImage(forms.ModelForm):
    brand_name = forms.CharField(label="Marka", max_length=150, widget=forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'Marka'}))
    description = forms.CharField(label="Marka Açıklama",required=False, max_length=350, widget=forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'Marka Açıklama'}))
    top_brand = forms.BooleanField(label = "Top", required=False)

    class Meta:
        model = Brand
        fields = ('brand_name','description','top_brand')

class BrandImageForm(forms.ModelForm):
    brand_image = forms.ImageField(label="", required=False, widget=forms.ClearableFileInput(attrs={'multiple':'multiple', 'class':'file-input', 'data-fouc':True}))
    class Meta:
        model = BrandImage
        fields = ('brand_image',)
