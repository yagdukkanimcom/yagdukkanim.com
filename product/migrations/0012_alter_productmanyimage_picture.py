# Generated by Django 3.2.5 on 2021-08-03 08:26

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('product', '0011_auto_20210714_1349'),
    ]

    operations = [
        migrations.AlterField(
            model_name='productmanyimage',
            name='picture',
            field=models.ImageField(blank=True, null=True, upload_to='product', verbose_name='Image'),
        ),
    ]
