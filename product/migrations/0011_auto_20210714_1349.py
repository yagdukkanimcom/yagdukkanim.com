# Generated by Django 3.1.7 on 2021-07-14 13:49

import ckeditor.fields
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('product', '0010_auto_20210713_1216'),
    ]

    operations = [
        migrations.AddField(
            model_name='product',
            name='product_tags',
            field=models.CharField(blank=True, max_length=500, null=True),
        ),
        migrations.AlterField(
            model_name='product',
            name='product_code',
            field=models.CharField(blank=True, max_length=100, null=True),
        ),
        migrations.AlterField(
            model_name='product',
            name='product_detail',
            field=ckeditor.fields.RichTextField(blank='False', max_length=8000, null='True'),
        ),
        migrations.AlterField(
            model_name='product',
            name='product_name',
            field=models.CharField(blank=True, max_length=200, null=True),
        ),
        migrations.AlterField(
            model_name='product',
            name='product_preparation_time',
            field=models.CharField(blank=True, max_length=25, null=True),
        ),
    ]
