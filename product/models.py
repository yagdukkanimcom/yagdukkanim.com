from django.db import models
from PIL import Image
from io import BytesIO
from django.core.files.uploadedfile import InMemoryUploadedFile
import sys
from unidecode import unidecode
from django.template.defaultfilters import slugify
from datetime import datetime
from category.models import Category, SubCategory, SubCategory2, Size, Color
from ckeditor.fields import RichTextField
from category.models import Color, Size


class Brand(models.Model):
    brand_name = models.CharField(max_length=50)
    slug = models.SlugField(null=True, unique=True, editable=False)
    description = models.CharField(max_length=350, blank=True, null=True)
    top_brand = models.BooleanField(default = False)

    def __str__(self):
        return self.brand_name

    def get_brand_image(self):
        return self.brand_image.all()


    def get_unique_slug(self):
        sayi=0
        slug = slugify(unidecode(self.brand_name))
        new_slug=slug
        while Brand.objects.filter(slug=new_slug).exists():
            sayi+=1
            new_slug="%s-%s"%(slug,sayi)
        slug = new_slug
        return slug

    def save(self, *args, **kwargs):
        if self.id is None:
            brand_name = self.get_unique_slug()
            self.slug = slugify(unidecode(brand_name))
        else:
            brand=Brand.objects.get(slug=self.slug)
            if brand.brand_name != self.brand_name:
                self.slug=self.get_unique_slug()
        super(Brand,self).save()

    class Meta:
        verbose_name_plural = "Marka"


class Product(models.Model):
    product_name = models.CharField(max_length=200, blank=True, null=True)
    product_detail = RichTextField(max_length=8000, blank="False", null="True")
    product_brand= models.ForeignKey(Brand, related_name='productbrand',on_delete=models.CASCADE, blank=True, null=True)
    product_price = models.FloatField(blank=True, null=True)
    product_discount_rate =  models.FloatField(blank=True, null=True)
    product_KDV_rate =  models.FloatField(blank=True, null=True)
    product_discount_price =  models.FloatField(blank=True, null=True)
    product_price_end = models.FloatField(blank=True, null=True)
    product_code = models.CharField(max_length=100, blank=True, null=True)
    product_total_quantity = models.IntegerField(blank=True, null=True)
    product_special = models.BooleanField(default = False)
    product_popular = models.BooleanField(default = False)
    product_sale = models.BooleanField(default = False)
    product_preparation_time = models.CharField(max_length=25, blank=True, null=True)
    product_tags = models.CharField(max_length=500, blank=True, null=True)

    category=models.ForeignKey(Category, related_name='category',on_delete=models.CASCADE, blank=True, null=True)
    sub_category = models.ForeignKey(SubCategory, on_delete=models.CASCADE, blank=True, null=True)
    sub_category2 = models.ForeignKey(SubCategory2, on_delete=models.CASCADE, blank=True, null=True)

    creation_date = models.DateField(default=datetime.now, blank=True)
    slug = models.SlugField(max_length=200, null=True, unique=True, editable=False)


    class Meta:
        verbose_name_plural = "Product"
        ordering=['product_name']

    def get_product_stock(self):
        return self.productstock.all()

    def get_product_image(self):
        return self.productimage.all()

    def get_product_many_image(self):
        return self.productmanyimage.all()

    def __str__(self):
        return self.product_name # str(format(int(passenger_male_count)/int(passenger_count), '.2f'))

    def discount_price(self):
        self.product_discount_price = format(self.product_price - self.product_price * (self.product_discount_rate / 100), '.2f')
        return self.product_discount_price

    def get_unique_slug(self):
        sayi=0
        slug = slugify(unidecode(self.product_name))
        new_slug=slug
        while Product.objects.filter(slug=new_slug).exists():
            sayi+=1
            new_slug="%s-%s"%(slug,sayi)
        slug = new_slug
        return slug

    def save(self, *args, **kwargs):
        if self.id is None:
            product_name = self.get_unique_slug()
            self.slug = slugify(unidecode(product_name))
        else:
            product = Product.objects.get(slug=self.slug)
            if product.product_name != self.product_name:
                self.slug=self.get_unique_slug()
        try:
            super(Product,self).save()
        except Exception as ex:
            pass

class ProductImage(models.Model):
    product_image = models.ImageField(upload_to='product',verbose_name='Image', null=True, blank=True)
    product = models.ForeignKey(Product, related_name='productimage',on_delete=models.CASCADE, blank=True, null=True)

    def get_image_url(self):
        return self.product_image.url



    def save(self, *args, **kwargs):
        imageTemproary = Image.open(self.product_image)
        #imageTemproary1 = imageTemproary.convert('RGB')
        outputIoStream = BytesIO()
        w, h = imageTemproary.size
        imageTemproaryResized = imageTemproary.resize( (int(w/1), int(h/1))  )
        imageTemproaryResized.save(outputIoStream , format='PNG', quality=85)
        outputIoStream.seek(0)
        self.product_image = InMemoryUploadedFile(outputIoStream,'ImageField', "%s.png" %self.product_image.name.split('.')[0], 'image/png', sys.getsizeof(outputIoStream), None)
        super(ProductImage, self).save(*args, **kwargs)

    class Meta:
        verbose_name_plural = "Company Photo"

class ProductManyImage(models.Model):
    picture = models.ImageField(upload_to='product',verbose_name='Image', null=True, blank=True)
    product = models.ForeignKey(Product, related_name='productmanyimage',on_delete=models.CASCADE, blank=True, null=True)


class ProductStock(models.Model):
    product = models.ForeignKey(Product, related_name='productstock',on_delete=models.CASCADE, blank=True, null=True)
    product_quantity = models.CharField(max_length=70, null=True, blank=True)
    product_size = models.ForeignKey(Size, related_name='productsize',on_delete=models.CASCADE, blank=True, null=True)
    creationDate = models.DateField(auto_now=True, blank=True, null=True)


    def _str_(self):
        return self.product.product_name

    class Meta:
        verbose_name_plural = "Product Stock"

class ProductsComments(models.Model):
    products = models.ForeignKey(Product, related_name='productcomments',on_delete=models.CASCADE, blank=True, null=True)
    senders_name = models.CharField(max_length=150, blank = True, null=True)
    senders_mail = models.EmailField(max_length=150, null=True)
    point = models.IntegerField(blank=True, null=True)
    text = models.CharField(max_length=1000, null=True)
    creationDate = models.DateField(auto_now=True, blank=True, null=True)


    def __str__(self):
        return self.senders_name

    class Meta:
        verbose_name_plural = "Ürün Yorumları"


class BrandImage(models.Model):
    brand_image = models.ImageField(upload_to='brand',verbose_name='Image', null=True, blank=True)
    brand = models.ForeignKey(Brand, related_name='brand_image',on_delete=models.CASCADE, blank=True, null=True)

    def get_image_url(self):
        return self.brand_image.url

    def save(self, *args, **kwargs):
        imageTemproary = Image.open(self.brand_image)
        imageTemproary1 = imageTemproary.convert('RGB')
        outputIoStream = BytesIO()
        w, h = imageTemproary.size
        imageTemproaryResized = imageTemproary.resize( (int(w/1), int(h/1))  )
        imageTemproaryResized.save(outputIoStream , format='JPEG', quality=85)
        outputIoStream.seek(0)
        self.brand_image = InMemoryUploadedFile(outputIoStream,'ImageField', "%s.jpg" %self.brand_image.name.split('.')[0], 'image/jpeg', sys.getsizeof(outputIoStream), None)
        super(BrandImage, self).save(*args, **kwargs)

    class Meta:
        verbose_name_plural = "Brand Fotoğrafı"
