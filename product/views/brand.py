from django.shortcuts import render, get_object_or_404, redirect
from django.contrib import messages
from ..models import Brand , BrandImage
from ..forms import BrandForm , BrandImageForm , BrandUpdateFormForImage

def delete_brand(request,pk):
    brand = get_object_or_404(Brand, pk=pk)
    brand.delete()
    messages.success(request, 'Marka Silindi.')
    return redirect('product:list-brand')

def detail_brand(request, pk):
    brand = get_object_or_404(Brand, pk=pk)
    context = {'brand': brand, 'pageheader': 'Marka Detay'}
    return render(request, 'adminpanel/brand/detail_brand.html', context)

def list_brand(request):
    brands = Brand.objects.all()
    context = {'brands': brands, 'pageheader': 'Markalar'}
    return render(request, 'adminpanel/brand/list_brand.html', context)

def update_brand(request,pk):
    brand = get_object_or_404(Brand, pk=pk)
    form = BrandForm(instance=brand, data=request.POST or None)
    if request.POST:
        if form.is_valid():
            form.save()
            messages.success(request, 'Marka Güncelleme Başarılı.')
            return redirect('product:list-brand')
        else:
            messages.error(request, 'Marka Güncelleme Başarısız...!!')

    context = {'form': form, 'pageheader': 'Marka Güncelle'}
    return render(request, 'adminpanel/brand/update_brand.html', context)

def add_brand(request):
    if request.POST:
        form = BrandForm(request.POST)
        imageForm = BrandImageForm(request.POST, request.FILES)
        files = request.FILES.getlist('brand_image')

        if form.is_valid() :
            brand = form.save()
            if imageForm.is_valid() and files:
                numberOfPhotosToUpload = 8
                countOfPhotosUploaded = 0
                for f in files:
                    countOfPhotosUploaded +=1
                    uploadedPhoto = BrandImage(brand=brand,brand_image=f)
                    if countOfPhotosUploaded <= numberOfPhotosToUpload:
                        uploadedPhoto.save()

            messages.success(request, 'Marka Ekleme Başarılı.')
            return redirect('product:list-brand')
        
        else:
            messages.error(request, 'Marka Ekleme Başarısız...!!')
    else:
        form = BrandForm()
        imageForm = BrandImageForm(request.POST, request.FILES)

    context = {'form': form, 'pageheader': 'Marka Ekle', "imageForm":imageForm, }
    return render(request, 'adminpanel/brand/add_brand.html', context)


def update_brand_image(request, pk):
    brand = get_object_or_404(Brand, pk=pk)
    form = BrandUpdateFormForImage(instance=brand, data=request.POST or None)

    imageQuery = []
    for img in brand.get_brand_image():
        imageQuery.append(BrandImage.objects.get(pk=img.pk))
        BrandImage.objects.get(pk=img.pk).delete()

    image_sayisi = 8 - len(imageQuery)
    imageForm = BrandImageForm(data=request.POST or None, files=request.FILES or None)

    if request.POST:

        imageForm = BrandImageForm(data=request.POST,files=request.FILES)
        files = request.FILES.getlist('brand_image')
        if  form.is_valid() and imageForm.is_valid():
            brand = form.save()

            countOfPhotosUploaded = 0
            for f in files:
                countOfPhotosUploaded +=1
                uploadedPhoto = BrandImage(brand=brand,brand_image=f)
                if countOfPhotosUploaded <= image_sayisi:
                    uploadedPhoto.save()

            return redirect('product:list-brand')
        else:
            messages.error(request, 'Marka Logo Güncelleme Başarısız...!!')

        messages.success(request, 'Marka Logo Güncelleme Başarılı.')
        return redirect('product:list-brand')

    return render(request, 'adminpanel/brand/update_brand_image.html', context={'pageheader': 'Brand Foto Update','form':form, 'imageForm':imageForm })
