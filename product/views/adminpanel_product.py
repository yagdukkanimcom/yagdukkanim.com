from django.shortcuts import render, redirect, get_object_or_404
from ..forms import AddProductForm , ProductImageForm , AddProductUpdateForm 
from ..models import Product , ProductImage 
from django.contrib import messages
from accounts.views import get_auth
from django.contrib.auth import authenticate
from django.contrib.auth import update_session_auth_hash
from category.models import Category, SubCategory, SubCategory2


def add_product_view(request):
    flag, gidilecek_sayfa = get_auth(request)
    if flag:
        return redirect(gidilecek_sayfa)

    if request.POST:
        imageForm = ProductImageForm(request.POST, request.FILES)
        files = request.FILES.getlist('product_image')
        form = AddProductForm(request.POST)
        category_pk = request.POST.get('category_pk')
        category = get_object_or_404(Category, pk = category_pk)
        sub_category_pk = request.POST.get('sub_category_pk')
        sub_category = get_object_or_404(SubCategory, pk = sub_category_pk)
        # sub_category_2_pk = request.POST.get('sub_category_2_pk')

        if form.is_valid() and imageForm.is_valid() and files:
            product = form.save(commit=False)
            product.category=category
            product.sub_category=sub_category
            product.product_discount_price  = format(product.product_price - product.product_price * (product.product_discount_rate / 100), '.2f')
            product.product_price_end  = format(float(product.product_discount_price) + float(product.product_discount_price) * float(product.product_KDV_rate / 100), '.2f')
            product.save()

            numberOfPhotosToUpload = 8
            countOfPhotosUploaded = 0
            for f in files:
                countOfPhotosUploaded +=1
                uploadedPhoto = ProductImage(product=product,product_image=f)
                if countOfPhotosUploaded <= numberOfPhotosToUpload:
                    uploadedPhoto.save()

            messages.success(request, 'Ürün Kaydı Başarılı')
            return redirect('product:list-delete-product')
        else:
            messages.error(request, 'Ürün Kaydı Başarısız..!!')
            return redirect('product:list-delete-product')
    else:
        form = AddProductForm()
        imageForm = ProductImageForm(request.POST, request.FILES)
    
    categorys = Category.objects.all()

    context = {'pageheader': 'Ürün Ekle', 'categorys':categorys, 'form':form, "imageForm":imageForm}
    return render(request, 'adminpanel/product/add_product.html',context)



def update_product_image(request, pk):
    flag, gidilecek_sayfa = get_auth(request)
    if flag:
        return redirect(gidilecek_sayfa)
    product = get_object_or_404(Product, pk=pk)
    form = AddProductUpdateForm(instance=product, data=request.POST or None)

    imageQuery = []
    for img in product.get_product_image():
        imageQuery.append(ProductImage.objects.get(pk=img.pk))
        ProductImage.objects.get(pk=img.pk).delete()

    image_sayisi = 8 - len(imageQuery)


    imageForm = ProductImageForm(data=request.POST or None, files=request.FILES or None)



    if request.POST:

        imageForm = ProductImageForm(data=request.POST,files=request.FILES)
        categories = request.POST.getlist('categories')
        files = request.FILES.getlist('product_image')
        if  form.is_valid() and imageForm.is_valid():
            product = form.save()

            countOfPhotosUploaded = 0
            for f in files:
                countOfPhotosUploaded +=1
                uploadedPhoto = ProductImage(product=product,product_image=f)
                if countOfPhotosUploaded <= image_sayisi:
                    uploadedPhoto.save()

            return redirect('product:list-delete-product')
        else:
            messages.error(request, 'Ürün Güncelleme İşlemi Başarısız')

        messages.success(request, 'Ürün Güncelleme İşlemi Başarılı')
        return redirect('product:list-delete-product')
    else:
        messages.error(request, 'Ürün Güncelleme İşlemi Başarısız')


    return render(request, 'adminpanel/product/update_product_image.html', context={'pageheader': 'Ürün Fotoğraf Güncelle','form':form, 'imageForm':imageForm, })

def update_product_view(request, pk):
    flag, gidilecek_sayfa = get_auth(request)
    if flag:
        return redirect(gidilecek_sayfa)
    product = get_object_or_404(Product, pk=pk)

    form = AddProductForm(instance=product, data=request.POST or None)
    
    if request.POST:
        category_pk = request.POST.get('category_pk')
        category = get_object_or_404(Category, pk = category_pk)
        sub_category_pk = request.POST.get('sub_category_pk')
        sub_category = get_object_or_404(SubCategory, pk = sub_category_pk)

        if form.is_valid():
            product = form.save(commit=False)
            product.category=category
            product.sub_category=sub_category
            product.product_discount_price  = format(product.product_price - product.product_price * (product.product_discount_rate / 100), '.2f')
            product.product_price_end  = format(float(product.product_discount_price) + float(product.product_discount_price) * float(product.product_KDV_rate / 100), '.2f')

            product.save()

            messages.success(request, 'Ürün Güncelleme İşlemi Başarılı')
            return redirect('product:list-delete-product')
        else:
            messages.error(request, 'Ürün Güncelleme İşlemi Başarısız')

        messages.success(request, 'Ürün Güncelleme İşlemi Başarılı')
        return redirect('product:list-delete-product')
    else:
        messages.error(request, 'Ürün Güncelleme İşlemi Başarısız')

    categorys = Category.objects.all()

    return render(request, 'adminpanel/product/update_product.html', context={'categorys':categorys,'product':product, 'form':form, 'pageheader': 'Ürün Güncelle'})


def list_product_view(request):
    flag, gidilecek_sayfa = get_auth(request)
    if flag:
        return redirect(gidilecek_sayfa)
    products = Product.objects.all()
    product_image=""
    try:
        product_image = ProductImage.objects.all()[0]
    except Exception as ex:
        pass
    context = {'products':products, 'pageheader': 'Waren List','product_image':product_image}
    return render(request, 'adminpanel/product/list_products.html', context)

def delete_product_view(request, pk):
    flag, gidilecek_sayfa = get_auth(request)
    if flag:
        return redirect(gidilecek_sayfa)
    product = get_object_or_404(Product, pk=pk)
    product.delete()
    messages.success(request, 'Silme İşlemi Başarılı')
    return redirect('product:list-delete-product')


def list_product_new_view(request):
    flag, gidilecek_sayfa = get_auth(request)
    if flag:
        return redirect(gidilecek_sayfa)

    products = Product.objects.all()
    context = {'products':products, 'pageheader': 'Ürün Listesi'}

    if request.GET.get('product_name'):
        product_name_new = request.GET.get('product_name')
        products = Product.objects.filter(product_name__icontains = product_name_new)
        context['products'] = products

    if request.method == "POST":
        islem = request.POST.get('islem')
        print(islem)
        if request.POST.getlist('islem_product_list'):
            product_ids = request.POST.getlist('islem_product_list')
            if islem == "sil":
                for id in product_ids:
                    delete_product = Product.objects.get(id=id)
                    delete_product.delete()
            elif islem == "ozel":
                for id in product_ids:
                    sale_product = Product.objects.get(id=id)
                    if sale_product.product_sale == False:
                        sale_product.product_sale = True
                    else:
                        sale_product.product_sale = False
                    sale_product.save()
        return redirect('product:list-delete-product')

    return render(request, 'adminpanel/product/list_product_new.html', context)
