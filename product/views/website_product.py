from website.views import category
from django.shortcuts import render, redirect, get_object_or_404, HttpResponseRedirect
from ..forms import AddProductForm , ProductImageForm  , AddProductUpdateForm 
from ..models import Product , ProductImage, ProductsComments, ProductStock, Brand
from adminpanel.models import Mesajlar
from django.contrib import messages
from accounts.views import get_auth
from django.contrib.auth import authenticate
from django.contrib.auth import update_session_auth_hash
from datetime import datetime, timedelta
from category.models import Category, SubCategory
from order.models import ShopCart, ReceivedProduct, OrderProduct, IadeTalebi
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
import math
import xml.etree.ElementTree as ET

def product_detail(request, slug):
    product = get_object_or_404(Product, slug=slug)
    comments = ProductsComments.objects.filter(products=product)
    ort_comments = 0
    top_points = 0
    category_products = Product.objects.filter(category = product.category)

    for i in comments:
        top_points = top_points + i.point
        
    if top_points >0:
        ort_comments = top_points / comments.count()

    if request.POST:
        senders_name = request.POST.get('name')
        senders_mail=request.POST.get('email')
        tel = request.POST.get('telNumber')
        subject = request.POST.get('product')
        subject = "Produkt  -- " + subject
        text = product.product_name

        data = Mesajlar.objects.create(senders_name=senders_name,senders_mail=senders_mail,tel=tel,subject=subject, text=text)
        data.save()  

    context = {'product':product, 'comments':comments , 'ort_comments':ort_comments, 'category_products':category_products}
    return render(request,'front_end/product/product.html', context)

def all_products(request):
    products = Product.objects.all().order_by('product_name')

    page_number = request.GET.get('page')

    session_id = request.session.session_key

    if not request.session.exists(request.session.session_key):
        request.session.create() 
        session_id = request.session.session_key

    price_list=[]
    for i in products:
        price_list.append(i.product_price_end)
    try:
        max_price = math.ceil(max(price_list))
    except Exception as ex:
        max_price=""

    if request.POST:  
        sort = request.POST.get('sort')

        if not request.POST.get('min_price')==None: 
            min_price_filter = float(request.POST.get('min_price').split('%')[0])
            max_price_filter = float(request.POST.get('max_price').split('%')[0])

            min_result = int(round(float((max_price * min_price_filter) / 100)))
            max_result = int(round(float((max_price * max_price_filter) / 100)))

            products = Product.objects.filter(product_price_end__gte=min_result, product_price_end__lte=max_result).order_by('product_price_end')
            paginator = Paginator(products, 16)

        if not sort==None: 
            products = Product.objects.all().order_by(sort)
            paginator = Paginator(products, 16)

        page_obj = paginator.get_page(page_number)

        context = {'products':products, 'max_price':max_price, 'page_obj':page_obj }
        return render(request,'website/all_product.html', context)

    paginator = Paginator(products, 16)
    page_obj = paginator.get_page(page_number)

    context = { 'products':products, 'max_price':max_price, 'page_obj':page_obj }
    return render(request,'website/all_product.html', context)

def brand_filter_products(request, slug):

    brand = get_object_or_404(Brand, slug = slug)
    products = Product.objects.filter(product_brand = brand)
    
    price_list=[]
    for i in products:
        price_list.append(i.product_price_end)
    try:
        max_price = math.ceil(max(price_list))
    except Exception as ex:
        max_price=""
    
    if request.POST:  
        sort = request.POST.get('sort')

        if not request.POST.get('min_price')==None: 
            min_price_filter = float(request.POST.get('min_price').split('%')[0])
            max_price_filter = float(request.POST.get('max_price').split('%')[0])
            deger = (int(max_price) * int(min_price_filter)) / 100
            deger2 = (int(max_price) * int(max_price_filter)) / 100
            min_result = int(round(float(deger)))
            max_result = int(round(float(deger2)))
            
            products = Product.objects.filter(product_brand=brand,product_price_end__gte=min_result, product_price_end__lte=max_result).order_by('product_price_end')
            page_number = request.GET.get('page')
            paginator = Paginator(products, 16)
            page_obj = paginator.get_page(page_number)

        context = {'page_obj':page_obj, 'max_price':max_price, 'brand':brand, }
        return render(request,'website/shop.html', context)

    page_number = request.GET.get('page')
    paginator = Paginator(products, 16)
    page_obj = paginator.get_page(page_number)

    context = { 'max_price':max_price,'brand':brand, 'page_obj':page_obj,}
    return render(request,'website/shop.html', context)

def category_filter_products(request, slug):

    try:
        sub_category = ""
        category = get_object_or_404(Category, slug=slug)
        products = Product.objects.filter(category = category)
    except Exception as ex:
        sub_category = get_object_or_404(SubCategory, slug=slug)
        category = sub_category.category
        products = Product.objects.filter(sub_category = sub_category)

    price_list=[]
    for i in products:
        price_list.append(i.product_price_end)
    try:
        max_price = math.ceil(max(price_list))
    except Exception as ex:
        max_price=""
    
    if request.POST:  
        sort = request.POST.get('sort')
        print(sort)

        if not request.POST.get('min_price')==None: 
            min_price_filter = float(request.POST.get('min_price').split('%')[0])
            max_price_filter = float(request.POST.get('max_price').split('%')[0])
            deger = (int(max_price) * int(min_price_filter)) / 100
            deger2 = (int(max_price) * int(max_price_filter)) / 100
            min_result = int(round(float(deger)))
            max_result = int(round(float(deger2)))
            
            products = Product.objects.filter(category=category,product_price_end__gte=min_result, product_price_end__lte=max_result).order_by('product_price_end')
        if sort:
            if sort == "name_a_z":
                products = products.order_by('product_name')
            elif sort == "name_z_a":
                products = products.order_by('-product_name')
            elif sort == "price_low_to_high":
                products = products.order_by('product_price_end')
            elif sort == "price_high_to_low":
                products = products.order_by('-product_price_end')

        page_number = request.GET.get('page')
        paginator = Paginator(products, 2)
        page_obj = paginator.get_page(page_number)

        context = {'page_obj': page_obj, 'max_price': max_price, 'category': category, 'sort':sort, 'sub_category':sub_category }
        return render(request,'front_end/category/category.html', context)

    page_number = request.GET.get('page')
    paginator = Paginator(products, 2)
    page_obj = paginator.get_page(page_number)

    context = {'page_obj':page_obj, 'max_price':max_price, 'category':category, 'sub_category':sub_category}
    return render(request,'front_end/category/category.html', context)

def popup_product_detail (request, slug):
    product = Product.objects.get(slug = slug)
    context = { 'product':product}
    return render(request,'website/all_product.html', context)

def search_view(request):

    if request.POST:
        product_name = request.POST.get('product_name')
        products = Product.objects.filter(product_name__icontains=product_name)

        try:
            client = request.user.client
        except Exception as ex:
            client = None
        
        product_total = Product.objects.filter(product_name__icontains=product_name).count()


        paginator = Paginator(products, 16)
        page = request.GET.get('page')
        try:
            product_list = paginator.page(page)
        except PageNotAnInteger:
            # If page is not an integer, deliver first page.
            product_list = paginator.page(1)
        except EmptyPage:
            # If page is out of range (e.g. 9999), deliver last page of results.
            product_list = paginator.page(paginator.num_pages)
        page_obj = paginator.get_page(page)



        context = {'product_total':product_total,'products':products, 
                    'product_list':product_list,'page_obj':page_obj,

                    }
        return render(request, 'website/shop.html', context)
    
    price_list=[]
    for i in products:
        price_list.append(i.product_price_end)
    if products:
        max_price = math.ceil(max(price_list))
    else:
        max_price =0 


    paginator = Paginator(products, 16)
    page = request.GET.get('page')
    try:
        product_list = paginator.page(page)
    except PageNotAnInteger:
        # If page is not an integer, deliver first page.
        product_list = paginator.page(1)
    except EmptyPage:
        # If page is out of range (e.g. 9999), deliver last page of results.
        product_list = paginator.page(paginator.num_pages)
    page_obj = paginator.get_page(page)



    context = {'product_total':product_total,'products':products, 
                'max_price':max_price, 
                'product_list':product_list,'page_obj':page_obj,
                }
    return render(request,'website/shop.html', context)

def add_comment (request, slug):
    product = get_object_or_404(Product, slug=slug)
    url = request.META.get('HTTP_REFERER')
    if request.POST:
        senders_name = request.POST.get('name')
        point = request.POST.get('rating') 
        comment=request.POST.get('comment')
        point = int(point)
        print(point)
        data = ProductsComments.objects.create(products=product, senders_name=senders_name, point=point, text = comment)
        data.save()  
    
    return HttpResponseRedirect(url)
