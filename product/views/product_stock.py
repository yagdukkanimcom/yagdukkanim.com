from django.shortcuts import render, redirect, get_object_or_404
from ..forms import AddProductStockForm  
from ..models import ProductStock, Product
from django.contrib import messages
from accounts.views import  get_auth
from django.contrib.auth import authenticate
from django.contrib.auth import update_session_auth_hash
from category.models import Size, Color


def deneme(request):
    pass
    # size = Size.objects.all()

    # return render(request, 'website/index.html', context = {'sizess': size})


    # products = Product.objects.all()
    # for i in products:
    #     i.product_size = ""
    #     for j in i.get_product_stock():
    #         a = j.product_size.size_name
    #         if i.product_size == "":
    #             i.product_size = a
    #         else:
    #             i.product_size = i.product_size + "-" + a
    #         i.save()
    
    
    #return redirect('website:index')


def add_product_stock(request):
    flag, gidilecek_sayfa = get_auth(request)
    if flag:
        return redirect(gidilecek_sayfa)

    if request.POST:
        form = AddProductStockForm(request.POST)

        if form.is_valid() :
            product_stock = form.save(commit=False)
            eklenicek_urun_miktari = int(float(product_stock.product_quantity))
            product_stock.save()

            product = Product.objects.get(productstock=product_stock)
            try:
                product.product_total_quantity = int(product.product_total_quantity) + eklenicek_urun_miktari
            except Exception as ex:
                product.product_total_quantity = 0
                product.product_total_quantity = int(product.product_total_quantity) + eklenicek_urun_miktari
            
            product.save()

            messages.success(request, 'Stock Ekleme İşlemi Başarılı')
            return redirect('product:list-product-stock')
        else:
            messages.error(request, 'Stock Ekleme İşlemi Başarısız..!!')
            return redirect('product:list-product-stock')
    else:
        form = AddProductStockForm()

    context = {'pageheader': 'Stock Ekle ','form':form }
    return render(request, 'adminpanel/product_stock/add_product_stock.html',context)

def update_product_stock(request, pk):
    flag, gidilecek_sayfa = get_auth(request)
    if flag:
        return redirect(gidilecek_sayfa)
    
    eski_product_stock = get_object_or_404(ProductStock, pk=pk)
    eski_quantity = int(eski_product_stock.product_quantity)

    form = AddProductStockForm(instance=eski_product_stock, data=request.POST or None)
    
    if request.POST:

        if form.is_valid():
            product_stock = form.save(commit=False)
            dusulecek_miktar = eski_quantity - int(float(product_stock.product_quantity))
            product_stock.save()

            product = Product.objects.get(productstock=product_stock)
            product.product_total_quantity = int(product.product_total_quantity) - dusulecek_miktar
            product.save()

            messages.success(request, 'Stock Güncelleme İşlemi Başarılı')
            return redirect('product:list-product-stock')
        else:
            messages.error(request, 'Stock Güncelleme İşlemi Başarısız')
            return redirect('product:list-product-stock')

    return render(request, 'adminpanel/product_stock/update_product_stock.html', context={'eski_product_stock':eski_product_stock, 'form':form, 'pageheader': 'Stock Güncelle' })


def list_product_stock(request):
    flag, gidilecek_sayfa = get_auth(request)
    if flag:
        return redirect(gidilecek_sayfa)

    product_stocks = ProductStock.objects.all()

    context = {'product_stocks':product_stocks, 'pageheader': 'Stock Listele' }
    return render(request, 'adminpanel/product_stock/list_product_stock.html', context)

def delete_product_stock(request, pk):
    flag, gidilecek_sayfa = get_auth(request)
    if flag:
        return redirect(gidilecek_sayfa)
    product_stock = get_object_or_404(ProductStock, pk=pk)
    product_stock.delete()
    messages.success(request, 'Silme İşlemi Başarılı')
    return redirect('product:list-product-stock')

