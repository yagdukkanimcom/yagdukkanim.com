from django.shortcuts import render, redirect, get_object_or_404
from django.contrib import messages
from category.models import Category, SubCategory, SubCategory2


def load_ending(request):

    category_pk = request.GET.get('category_pk')
    category = get_object_or_404(Category, pk = category_pk)

    sub_categories = SubCategory.objects.filter(category=category)
    print('****************')
    print(sub_categories)
    
    return render(request, 'adminpanel/product/load_ending.html', context={'sub_categories': sub_categories})

def load_categories(request):
    sub_category_pk = request.GET.get('sub_category_pk')
    sub_category = get_object_or_404(SubCategory, pk = sub_category_pk)

    sub_categories_2 = SubCategory2.objects.filter(sub_category=sub_category)

    return render(request, 'adminpanel/product/load_categories.html', context={'sub_categories_2': sub_categories_2})
