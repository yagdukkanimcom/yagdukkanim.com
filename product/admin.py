from django.contrib import admin
from .models import Product, ProductManyImage, ProductStock,ProductsComments,Brand

admin.site.register(Product)
admin.site.register(ProductManyImage)
admin.site.register(ProductStock)
admin.site.register(ProductsComments)
admin.site.register(Brand)