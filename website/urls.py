from django.contrib import admin
from django.urls import path,include
from . import views

app_name = 'adminpanel'

urlpatterns = [
path('',views.home,name='home'),
path('hakkimizda/',views.about,name='about'),
path('iletisim/',views.contact,name='contact'),
path('urunler/',views.product,name='product'),
path('kategori/',views.category,name='category'),
path('giris/',views.login,name='login'),
path('kayit-ol/',views.register,name='register'),
path('favorilerim/',views.wishlist,name='wishlist'),
path('hesabım/',views.account,name='account'),
path('sepet/',views.cart,name='cart'),
path('odeme/',views.checkout,name='checkout'),
path('cerez-politikasi/',views.cookie_policy,name='cookie_policy'),
]
