from django.shortcuts import render
from accounts.models import CompanyImage
from django.contrib.auth import authenticate, login, logout
from adminpanel.models import SSS
from adminpanel.models import  Mesajlar
from accounts.forms import  LoginForm
from django.shortcuts import redirect

# Create your views here.
def home(request):
    context = {}
    return render(request,"front_end/home/index.html",context)



def cart(request):
        context = {}
        return render(request,"front_end/cart/cart.html",context)




def about(request):
    sss = SSS.objects.all()
    context = {'sss':sss}
    return render(request,"front_end/about/about.html",context)
def product(request):
    context = {}
    return render(request,"front_end/product/product.html",context)
def category(request):
    context = {}
    return render(request,"front_end/category/category.html",context)

def login(request): 
    if request.user.is_authenticated:
        try:
            superadmin = request.user.superadmin
            return redirect('adminpanel:index')
        except Exception as e:
            pass
        try:
            kullanici = request.user.kullanici
            return redirect('adminpanel:index')
        except Exception as e:
            pass
        try:
            musteri = request.user.client
            return redirect('website:index')
        except Exception as e:
            pass

    if request.POST:
        login_form = LoginForm(request.POST)
        if login_form.is_valid():
            username = request.POST['username']
            password = request.POST['password']

            user1 = authenticate(username=username, password=password)
            if user1:
                try:
                    superadmin = user1.superadmin
                    login(request, user1)
                    print('superadmin')
                    messages.success(request, 'Giriş İşlemi Başarılı')
                    return redirect('adminpanel:index')
                except Exception as e:
                    pass
                try:
                    kullanici = user1.kullanici
                    login(request, user1)
                    print('kullanıcı')
                    messages.success(request, 'Giriş İşlemi Başarılı')
                    return redirect('adminpanel:index')
                except Exception as e:
                    pass
                try:
                    client = user1.client
                    print('client')
                    login(request, user1)
                    return redirect('musteri:musteri-ana-sayfa')
                except Exception as e:
                    pass
            else:
                messages.error(request, 'Giriş İşlemi Başarısız')
        else:
            messages.error(request, 'Giriş İşlemi Başarısız')
    else:
        login_form = LoginForm()
    try:
        company_info = CompanyInfo.objects.all()[0]
        company_image = CompanyImage.objects.all()[0]
    except Exception as e:
        company_info = ""
        company_image = ""
    context = {'login_form': login_form, 'company_info':company_info, 'company_image':company_image}
    return render(request, 'front_end/login/login.html', context)


def register(request):
    context = {}
    return render(request,"front_end/register/register.html",context)
def wishlist(request):
    context = {}
    return render(request,"front_end/wishlist/wishlist.html",context)
def account(request):
    context = {}
    return render(request,"front_end/account/account.html",context)
def checkout(request):
        context = {}
        return render(request,"front_end/checkout/checkout.html",context)
def contact(request):
        context = {}
        if request.POST:
            senders_name1 = request.POST.get("senders_name")
            senders_mail1 = request.POST.get("senders_mail")
            subject1 = request.POST.get("subject")

            mesajKaydet = Mesajlar.objects.create(senders_name=senders_name1,senders_mail=senders_mail1,subject=subject1)
            mesajKaydet.save()
        return render(request,"front_end/contact/contact.html",context)
def cookie_policy(request):
        context = {}
        return render(request,"front_end/other-pages/cookie_policy.html",context)
