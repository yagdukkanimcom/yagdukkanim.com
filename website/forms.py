from django import forms
from .models import Transfer
from accounts.models import Client
from django.contrib.auth.models import User


class UpdateClientForm(forms.ModelForm):
    tckn = forms.CharField(max_length=60, widget=forms.TextInput(attrs={'class': 'form-control', 'onkeypress':'if ( isNaN( String.fromCharCode(event.keyCode) )) return false;', 'maxLength':11, 'oninput':'javascript: if (this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);'}))
    class Meta:
        model = Client
        fields = ['tckn']

class UpdateUserInfoForm(forms.ModelForm):
    first_name = forms.CharField(max_length=60, widget=forms.TextInput(attrs={'class': 'form-control'}))
    last_name = forms.CharField(max_length=60, widget=forms.TextInput(attrs={'class': 'form-control'}))
    email = forms.EmailField(max_length=60, widget=forms.TextInput(attrs={'class': 'form-control', 'type':'email'}))
    username = forms.CharField(max_length=60, widget=forms.TextInput(attrs={'class': 'form-control' ,'readonly':'true'}))
    class Meta:
        model = User
        fields = ['first_name', 'last_name', 'email', 'username']