# Generated by Django 3.1.7 on 2021-07-09 11:44

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('client', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Client',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('tckn', models.CharField(blank=True, max_length=11, null=True)),
                ('slug', models.SlugField(editable=False, null=True, unique=True)),
                ('tel', models.IntegerField(blank=True, null=True)),
                ('adres', models.CharField(blank=True, max_length=350, null=True)),
                ('user', models.OneToOneField(on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL)),
            ],
            options={
                'verbose_name_plural': 'Müşteri',
            },
        ),
        migrations.CreateModel(
            name='ClientCompany',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('vergi_dairesi', models.CharField(blank=True, max_length=100, null=True)),
                ('vergi_no', models.CharField(blank=True, max_length=25, null=True)),
                ('firma_unvani', models.CharField(blank=True, max_length=100, null=True)),
                ('company_address', models.CharField(blank=True, max_length=250, null=True)),
                ('company_telephone', models.CharField(blank=True, max_length=250, null=True)),
                ('client', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, to='client.client', verbose_name='clientcompany')),
            ],
            options={
                'verbose_name_plural': 'Kurumsal Müşteriler',
            },
        ),
        migrations.RemoveField(
            model_name='musteri',
            name='user',
        ),
        migrations.DeleteModel(
            name='MusteriCompany',
        ),
        migrations.AlterField(
            model_name='address',
            name='client',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='musteriaddress', to='client.client'),
        ),
        migrations.DeleteModel(
            name='Musteri',
        ),
    ]
