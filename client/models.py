from django.db import models
from django.contrib.auth.models import User
from django.template.defaultfilters import slugify
from unidecode import unidecode

class Client(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    tckn = models.CharField(max_length=11, blank=True, null=True)
    slug = models.SlugField(null=True, unique=True, editable=False)
    tel =  models.IntegerField(blank=True, null=True)
    adres = models.CharField(max_length=350, blank=True, null=True)

    def __str__(self):
        return self.user.email

    class Meta:
        verbose_name_plural = "Müşteri"

    def get_unique_slug(self):
        sayi=0
        slug = slugify(unidecode(self.user.username))
        new_slug=slug
        while Client.objects.filter(slug=new_slug).exists():
            sayi+=1
            new_slug="%s-%s"%(slug,sayi)
        slug = new_slug
        return slug

    def save(self, *args, **kwargs):
        if self.id is None:
            client_username = self.get_unique_slug()
            self.slug = slugify(unidecode(client_username))
        else:
            client=Client.objects.get(slug=self.slug)
            if client.user.username != self.user.username:
                self.slug=self.get_unique_slug()
        super(Client,self).save()

class ClientCompany(models.Model):
    vergi_dairesi = models.CharField(max_length=100, blank=True, null=True)
    vergi_no = models.CharField(max_length=25, blank=True, null=True)
    firma_unvani = models.CharField(max_length=100, blank=True, null=True)
    company_address = models.CharField(max_length=250, blank=True, null=True)
    company_telephone = models.CharField(max_length=250, blank=True, null=True)
    client = models.ForeignKey(Client, verbose_name="clientcompany", on_delete=models.SET_NULL, blank=True, null=True)

    def __str__(self):
        return str(self.firma_unvani)

    class Meta:
        verbose_name_plural = "Kurumsal Müşteriler"

class Address(models.Model):
    address_name = models.CharField(max_length=150, blank=True, null=True)
    country = models.CharField(max_length=150, blank=True, null=True)
    city = models.CharField(max_length=150, blank=True, null=True)
    district = models.CharField(max_length=150, blank=True, null=True)
    open_address = models.CharField(max_length=350, blank=True, null=True)
    address_tel =  models.CharField(max_length=50, blank=True, null=True)
    client = models.ForeignKey(Client, related_name='clientaddress', on_delete=models.SET_NULL, blank=True, null=True)

    def __str__(self):
        return str(self.address_name)

    address_name = models.CharField(max_length=150, blank=True, null=True)
    country = models.CharField(max_length=150, blank=True, null=True)
    city = models.CharField(max_length=150, blank=True, null=True)
    district = models.CharField(max_length=150, blank=True, null=True)
    open_address = models.CharField(max_length=350, blank=True, null=True)
    address_tel =  models.IntegerField(blank=True, null=True)
    client = models.ForeignKey(Client, related_name='musteriaddress', on_delete=models.SET_NULL, blank=True, null=True)

    def __str__(self):
        return str(self.address_name)