from django.urls import path
from django.conf.urls import url
from .views import adminpanel_musteri , website_musteri 

app_name = 'musteri'

urlpatterns = [

    path('musteri-listele/', adminpanel_musteri.musteri_listele_view, name="musteri-listele"),
    path('musteri-sil/<int:pk>/', adminpanel_musteri.musteri_sil_view, name="musteri-sil"),
    path('musteri-duzenle/<int:pk>/', adminpanel_musteri.musteri_duzenle_view, name="musteri-duzenle"),
    path('musteri-iade-talepleri-adminpanel/', adminpanel_musteri.musteri_iade_talebi_listele, name="adminpanel-musteri-iade-talepleri"),
    path('musteri-iade-talebi-duzenle/<int:pk>/', adminpanel_musteri.musteri_iade_talebi_duzenle, name="musteri-iade-talebi-duzenle"),

    path('musteri-ana-sayfa/', website_musteri.musteri_ana_sayfa_view, name="musteri-ana-sayfa"),
    path('musteri-kullanici-bilgileri/', website_musteri.musteri_kullanici_bilgileri_view, name="musteri-kullanici-bilgileri"),
    path('musteri-siparisler/', website_musteri.musteri_siparisler_view, name="musteri-siparisler"),
    path('musteri-iade-talepleri/', website_musteri.musteri_siparis_iade_talepleri, name="musteri-iade-talepleri"),
    path('musteri-siparis-iade-talebi/<int:pk>/', website_musteri.musteri_siparis_iade_talebi, name="musteri-siparis-iade-talebi"),
    path('musteri-parola-degistir/', website_musteri.musteri_parola_degistir, name="musteri-parola-degistir"),
    path('musteri-siparis-goruntule/<int:pk>/', website_musteri.siparis_goruntule_view, name="musteri-siparis-goruntule"),
    path('musteri-adres-guncelle/', website_musteri.musteri_adres_guncelle, name="musteri-adres-guncelle"),

]