
from django.shortcuts import render, redirect, get_object_or_404
from django.contrib import messages
from accounts.views import get_auth
from ..models import Client
from ..forms import MusteriDuzenlemeFormu
from order.models import IadeTalebi
from order.forms import IadeTalebiDuzenlemeFormu

def musteri_listele_view(request):
    flag, gidilecek_sayfa = get_auth(request)
    if flag:
        return redirect(gidilecek_sayfa)
    
    musteriler = Client.objects.all()

    context = { 'musteriler':musteriler ,'pageheader': 'Müşteriler'}

    return render(request, 'adminpanel/client/client_list.html', context)

def musteri_sil_view(request, pk):
    flag, gidilecek_sayfa = get_auth(request)
    if flag:
        return redirect(gidilecek_sayfa)
    
    musteri = get_object_or_404(Client, pk=pk)
    musteri.delete()
    messages.success(request, 'Silme İşlemi Başarılı')

    return redirect('musteri:musteri-listele')

def musteri_duzenle_view(request, pk):
    flag, gidilecek_sayfa = get_auth(request)
    if flag:
        return redirect(gidilecek_sayfa)
    
    musteri = get_object_or_404(Client, pk=pk)
    form = MusteriDuzenlemeFormu(instance=musteri, data=request.POST or None)
    if request.POST:
        if form.is_valid():
            form.save()
            messages.success(request, 'Güncelleme İşlemi Başarılı')
            return redirect('musteri:musteri-listele')
        else:
            messages.error(request, 'Güncelleme İşlemi Başarısız')
    context = {'form':form,'musteri':musteri, 'pageheader': 'Musteri Güncelle'}

    return render(request, 'adminpanel/client/client_update.html',context)

def musteri_iade_talebi_duzenle(request, pk):
    flag, gidilecek_sayfa = get_auth(request)
    if flag:
        return redirect(gidilecek_sayfa)
    
    iade_talebi = get_object_or_404(IadeTalebi, pk=pk)
    
    form = IadeTalebiDuzenlemeFormu(instance=iade_talebi, data=request.POST or None)
    if request.POST:
        if form.is_valid():
            form.save()
            messages.success(request, 'Güncelleme İşlemi Başarılı')
            return redirect('musteri:adminpanel-musteri-iade-talepleri')
        else:
            messages.error(request, 'Güncelleme İşlemi Başarısız')

    context = {'iade_talebi':iade_talebi, 'form': form, 'pageheader': 'İade Talebi Güncelle', }

    return render(request, 'adminpanel/siparis/iade_talebi_duzenle.html', context)

def musteri_iade_talebi_listele(request):
    flag, gidilecek_sayfa = get_auth(request)
    if flag:
        return redirect(gidilecek_sayfa)
    
    iade_talepleri=IadeTalebi.objects.all()


    context = { 'iade_talepleri':iade_talepleri ,'pageheader': 'İade Talepleri'}

    return render(request, 'adminpanel/siparis/iade_talebi_listele.html', context)
  