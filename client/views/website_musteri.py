
from django.shortcuts import render, get_object_or_404, redirect ,HttpResponseRedirect
from client.models import *
from accounts.forms import UserPasswordChangeForm
from accounts.models import *
from django.contrib.auth.models import User
from django.contrib.auth import update_session_auth_hash
from product.models import Product
from order.models import ReceivedProduct, IadeTalebi

def musteri_ana_sayfa_view (request):
    products = Product.objects.all()
    client = request.user.client
    context = { 'client':client, 'products':products, }
    return render(request,'website/musteri/musteri-ana-ekran.html', context)


def musteri_siparisler_view (request):
    products = Product.objects.all()
    client=request.user.client
    siparisler = ReceivedProduct.objects.filter(client=client)
    context = { 'client':client, 'siparisler':siparisler, 'products':products, }
    return render(request,'website/musteri/musteri-siparisler.html',context)

def siparis_goruntule_view(request, pk):
    siparis = get_object_or_404(ReceivedProduct, pk=pk)
    products = Product.objects.all()
    client=request.user.client
    context = { 'client':client, 'siparis':siparis, 'products':products, }
    return render(request, 'website/musteri/musteri-siparis-goruntule.html',context)

def musteri_kullanici_bilgileri_view (request ):
    products = Product.objects.all()
    client=request.user.client
    if request.POST:
        ad= request.POST.get('first_name')
        soyad=request.POST.get('last_name')
        tel=request.POST.get('tel')
        email=request.POST.get('email')
        adres=request.POST.get('adres')
        tckn=request.POST.get('tckn')

        firma_unvani=request.POST.get('firma_unvani')
        vergi_dairesi=request.POST.get('vergi_dairesi')
        vergi_no=request.POST.get('vergi_no')
        company_address=request.POST.get('company_address')
        company_telephone=request.POST.get('company_telephone')

        c = get_object_or_404(Client, pk = request.user.client.pk)
        c.tckn = tckn
        c.tel = tel
        c.adres = adres
        user = get_object_or_404(User, pk = request.user.pk)
        user.first_name = ad
        user.last_name = soyad
        user.email = email
        user.save()
        c.user = user

        try:
            client_company = get_object_or_404(ClientCompany, client = request.user.client)
        except Exception as ex:
            client_company = ClientCompany()

        client_company.firma_unvani = firma_unvani
        client_company.vergi_dairesi = vergi_dairesi
        client_company.vergi_no = vergi_no
        client_company.company_address = company_address
        client_company.company_telephone = company_telephone
        client_company.client = request.user.client
        client_company.save()
        c.save()

        return redirect('musteri:musteri-kullanici-bilgileri')

    try:
        company=ClientCompany.objects.filter(client=request.user.client)
        company_default = company[0]
    except Exception as ex:
        company = ""
        company_default=""

    try:
        address = Address.objects.filter(client=request.user.client)
        address_default = address[0]
    except Exception as ex:
        address_default = ""
        address = ""
    
    context = { 'client':client, 'products':products,
                'address':address, "address_default":address_default,
                'company':company,'company_default':company_default,
                }
    return render(request,'website/musteri/musteri-kullanici-bilgileri.html', context)

def musteri_adres_guncelle(request):
    products = Product.objects.all()
    
    client=request.user.client
    
    try:
        company=ClientCompany.objects.filter(client=request.user.client)
        company_default = company[0]
    except Exception as ex:
        company = ""
        company_default=""

    try:
        addressler = Address.objects.filter(client=request.user.client)
        address_default = addressler[0]
    except Exception as ex:
        address_default = ""
        addressler = ""

    if request.POST:
        address_name= request.POST.get('address_name')
        country=request.POST.get('country')
        city=request.POST.get('city')
        district=request.POST.get('district')
        address_tel=request.POST.get('address_tel')
        open_address=request.POST.get('open_address')

        try:
            pass
        except Exception as ex:
            pass
        address = Address.objects.filter(address_name=address_name, client = request.user.client)[0]
        address.city=city
        address.country=country
        address.district=district
        address.address_tel=address_tel
        address.open_address=open_address
        address.save()

    context = { 'client':client,'products':products,
                'address':addressler, "address_default":address_default,
                'company':company,'company_default':company_default,
                }
    return render(request,'website/musteri/musteri_adres_guncelle.html', context)


def musteri_siparis_iade_talebi(request, pk):
    products = Product.objects.all()
    client=request.user.client
    iade_talebi=get_object_or_404(ReceivedProduct, pk=pk)
    iade = IadeTalebi.objects.create(client=client, siparis = iade_talebi)
    iade.save()

    context = { 'client':client,'products':products, }
    return render(request,'website/musteri/musteri-ana-ekran.html', context)


def musteri_siparis_iade_talepleri(request):
    products = Product.objects.all()
    client=request.user.client
    iade_talepleri = IadeTalebi.objects.filter(client=client)
    context = { 'client':client, 'iade_talepleri':iade_talepleri,'products':products, }
    return render(request,'website/musteri/musteri_iade_talepleri.html',context)


def musteri_parola_degistir(request):
    products = Product.objects.all()
    client=request.user.client
    if not request.user.is_authenticated:
        return redirect('accounts:login')

    form = UserPasswordChangeForm(user=request.user, data=request.POST or None)
    if request.POST:
        if form.is_valid():
            new_password = form.cleaned_data.get('new_password')
            request.user.set_password(new_password)
            request.user.save()
            update_session_auth_hash(request, request.user)
            return redirect('musteri:musteri-parola-degistir')

    context = { 'form':form, 'pageheader': 'Kullanıcı Parola Değiştirme','client':client, 'products':products,
                }
    return render(request, 'website/musteri/musteri-parola-degistir.html', context)