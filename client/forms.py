from django import forms
from client.models import Client

class MusteriDuzenlemeFormu(forms.ModelForm):
    tel= forms.CharField(max_length=460, widget=forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'Telefonnummer des Kunden '}))

    class Meta:
        model = Client
        fields = ['tel']
